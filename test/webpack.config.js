const VueLoaderPlugin = require("vue-loader/lib/plugin")
const path = require("path")

const config = {
  mode: "none",
  devtool: false,
  entry: { index: "../src/main.js" },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{ loader: "css-loader" }]
      },
      {
        test: /\.less$/,
        use: [{ loader: "css-loader", options: { importLoaders: 1 } }, { loader: "less-loader" }]
      },
      {
        test: /\.styl(us)?$/,
        use: [{ loader: "css-loader", options: { importLoaders: 1 } }, { loader: "stylus-loader" }]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: { loader: "file-loader" }
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "img/[name].[ext]",
              limit: "300"
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.pug$/,
        oneOf: [
          // this applies to `<template lang="pug">` in Vue components
          {
            resourceQuery: /^\?vue/,
            use: ["pug-plain-loader"]
          },
          // this applies to pug imports inside JavaScript
          {
            use: ["raw-loader", "pug-plain-loader"]
          }
        ]
      },
      { test: /\.vue$/, loader: "vue-loader" }
    ]
  },
  resolve: {
    alias: {
      vue: "vue/dist/vue.min.js",
      "@": path.resolve(__dirname, "../src"),
      img: path.resolve(__dirname, "../src/img")
    },
    extensions: [".js", ".vue"]
  },
  plugins: [new VueLoaderPlugin()]
}

module.exports = config
