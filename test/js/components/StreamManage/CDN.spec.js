import { shallowMount, createLocalVue } from "@vue/test-utils"
import { expect } from "chai"
import Vuex from "vuex"
import CDN from "../../../../src/components/common/StreamManage/CDN.vue"
import vuexCasino from "../../../helper/vuex/casino.js"

const localVue = createLocalVue()
localVue.use(Vuex)

const resultRows = [
  {
    uid: "13444ae1559f4db9b3f04d01df672987",
    channelId: 99,
    address: "10.10.10.10",
    remark: "aaaa",
    defaultOption: false,
    locked: false
  },
  {
    uid: "6656b3e29b32476da44a7e6072ce2d26",
    channelId: 1,
    address: "10.10.100.213",
    remark: "我誰",
    defaultOption: true,
    locked: false
  },
  {
    uid: "7f6629f5d2544aeb99a93efd45cf5df2",
    channelId: 4,
    address: "10.10.100.213",
    remark: "拉拉",
    defaultOption: false,
    locked: false
  }
]

describe("Stream Manage Page - CDN tab", () => {
  let store = {}
  let wrapper = {}
  const $route = { path: "/main/StreamManage.html" }

  beforeEach(() => {
    store = new Vuex.Store(vuexCasino)
    wrapper = shallowMount(CDN, {
      store,
      localVue,
      mocks: {
        $route
      }
    })
  })

  it("has correct title text", () => {
    expect(wrapper.find(".stream-manage__tab-title").text()).to.eq("CDN管理")
  })

  it("has add new cdn button", () => {
    const target = wrapper.find(".stream-manage__tab-title-area .primary-button")
    expect(target.text()).to.eq("新增")
    expect(target.isVisible()).to.be.true
  })

  it("will render cdn list table", async () => {
    wrapper.vm.resultRows = resultRows
    await wrapper.vm.$nextTick()
    expect(wrapper.find(".report-table").isVisible()).to.be.true
    expect(wrapper.findAll(".report-table .report-table__row").length).to.eq(4)
  })

  it("has cdn table header columns", async () => {
    wrapper.vm.resultRows = resultRows
    await wrapper.vm.$nextTick()
    const targets = wrapper.findAll(".report-table .report-table__header-cell")
    expect(targets.isVisible()).to.be.true
    expect(targets.at(0).text()).to.be.eq("CDN說明")
    expect(targets.at(1).text()).to.be.eq("頻道")
    expect(targets.at(2).text()).to.be.eq("CDN位址")
    expect(targets.at(3).text()).to.be.eq("是否預設")
    expect(targets.at(4).text()).to.be.eq("是否啟用")
    expect(targets.at(5).text()).to.be.eq("操作")
  })

  it("will display data base on resultRows", async () => {
    wrapper.vm.resultRows = resultRows
    await wrapper.vm.$nextTick()
    const targets = wrapper.findAll(".report-table .stream-manage-cdn__row")
    const target = targets.at(1).findAll(".report-table__cell")

    expect(target.isVisible()).to.be.true
  })

  describe("Add CDN", () => {})

  describe("Edit CDN", () => {})
})
