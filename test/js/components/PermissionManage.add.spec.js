import { shallowMount, createLocalVue } from "@vue/test-utils"
import { expect } from "chai"
import Vuex from "vuex"
import vuexLoginToken from "../../helper/vuex/loginToken.js"
import vuexMenuList from "../../helper/vuex/menuList.js"
import AddPermissionManage from "../../../src/components/common/PermissionManage/AddPermissionRole.vue"

const localVue = createLocalVue()
localVue.use(Vuex)

describe("Permission Manage Page - Add", () => {
  let store = {}
  let wrapper = {}
  const $route = { path: "/main/PermissionManage.html" }

  beforeEach(() => {
    store = new Vuex.Store({ ...vuexLoginToken, ...vuexMenuList })
    wrapper = shallowMount(AddPermissionManage, {
      store,
      localVue,
      mocks: {
        $route
      },
      propsData: {
        isDisplay: true
      }
    })
  })

  it("has frame title", async () => {
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.isDisplay).to.be.true
    expect(wrapper.find("#labCreatInAddChar").text()).to.eq("新增角色")
  })
})
