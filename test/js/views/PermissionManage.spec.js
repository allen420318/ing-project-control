import { shallowMount, createLocalVue } from "@vue/test-utils"
import { expect } from "chai"
import Vuex from "vuex"
import vuexLoginToken from "../../helper/vuex/loginToken.js"
import PermissionManagePage from "../../../src/views/PermissionManage.vue"

const localVue = createLocalVue()
localVue.use(Vuex)

describe("Permission Manage Page", () => {
  let store = {}
  let wrapper = {}
  const $route = { path: "/main/PermissionManage.html" }

  beforeEach(() => {
    store = new Vuex.Store(vuexLoginToken)
    wrapper = shallowMount(PermissionManagePage, {
      store,
      localVue,
      mocks: {
        $route
      }
    })
  })

  it("has correct title text & id", () => {
    expect(wrapper.find("#labCharPermissionTitle").text()).to.eq("角色權限管理")
  })

  it("has add permission role button", () => {
    expect(wrapper.find("#btnAddChar").text()).to.eq("新增角色")
  })

  describe("will display add permission content", () => {
    beforeEach(() => {
      store = new Vuex.Store(vuexLoginToken)

      wrapper = shallowMount(PermissionManagePage, {
        store,
        localVue,
        mocks: {
          $route
        }
      })
    })

    it("clicked to display add permission content", async () => {
      wrapper.find("#btnAddChar").trigger("click")
      expect(wrapper.vm.isDisplayAddPermissionRole).to.be.true
      await wrapper.vm.$nextTick()
      const addPermissionRole = wrapper.find({ name: "AddPermissionRole" })
      expect(addPermissionRole.name()).to.eq("AddPermissionRole")
      expect(addPermissionRole.isVueInstance()).to.be.true
      expect(addPermissionRole.isVisible()).to.be.true
    })

    it("page breadcrumb changes", () => {
      wrapper.find("#btnAddChar").trigger("click")
      expect(wrapper.vm.navbarItems.length).to.eq(3)
      expect(wrapper.vm.navbarItems[0]).to.include({ title: "系統管理", islink: false, path: "" })
      expect(wrapper.vm.navbarItems[1].islink).to.be.true
      expect(wrapper.vm.navbarItems[1].title).to.eq("角色權限管理")
      expect(wrapper.vm.navbarItems[1].path).to.eq($route.path)
      expect(wrapper.vm.navbarItems[2]).to.include({ title: "新增角色", islink: false, path: "" })
    })
  })

  describe("will display permission role list on loaded", () => {
    let tableWapper = {}

    const apiResponse = {
      exitCode: true,
      version: "0.0.1",
      data: [
        { uid: "bae125e0a3354222ae37fd2f6b192650", name: "testName", entrance: "edevbo", remark: "testComment" },
        { uid: "ad719d7b7aa74aeea80ca9117ad7b81d", name: "all", entrance: "edevbo", remark: "" },
        { uid: "81fa51d69228477a87816e7c8f70c2a4", name: "dogd", entrance: "edevbo", remark: "" },
        { uid: "f287e9d1b5ab4bbd9d7a0276c6ff1629", name: "God", entrance: "edevbo", remark: "" }
      ]
    }

    beforeEach(() => {
      store = new Vuex.Store(vuexLoginToken)

      tableWapper = shallowMount(PermissionManagePage, {
        store,
        localVue,
        mocks: {
          $route
        }
      })

      tableWapper.setData({ resultRows: apiResponse.data })
    })

    it("will render table by api response", async () => {
      expect(tableWapper.vm.resultRows.length).to.eq(4)
      expect(tableWapper.vm.reportTableRows.length).to.eq(4)
      expect(tableWapper.vm.reportTableRows[0]).to.have.all.keys(["permissionRole", "comment", "operates"])

      await tableWapper.vm.$nextTick()

      const resultTable = tableWapper.find("#tabCharPermission")

      expect(resultTable.isVisible()).to.be.true

      const resultTableBodyRows = resultTable
        .findAll(".report-table__row")
        .filter(x => !x.contains(".report-table__header-cell"))

      expect(resultTableBodyRows.length).to.be.eq(4)
    })

    it("has 3 columns on each rows", async () => {
      await tableWapper.vm.$nextTick()

      const resultTableRows = tableWapper.find("#tabCharPermission").findAll(".report-table__row")
      const headerRow = resultTableRows.at(0)
      const headerRowColumns = headerRow.findAll(".report-table__header-cell")

      expect(headerRowColumns.length).to.be.eq(3)
      expect(headerRowColumns.at(0).contains("._role")).to.be.true
      expect(headerRowColumns.at(0).text()).to.eq("角色")
      expect(headerRowColumns.at(1).contains("._remarks")).to.be.true
      expect(headerRowColumns.at(1).text()).to.eq("備註")
      expect(headerRowColumns.at(2).contains("._operate")).to.be.true
      expect(headerRowColumns.at(2).text()).to.eq("操作")
    })

    it("has 2 action buttons on each body rows", async () => {
      await tableWapper.vm.$nextTick()

      const resultTableBodyRows = tableWapper
        .find("#tabCharPermission")
        .findAll(".report-table__row")
        .filter(x => !x.contains(".report-table__header-cell"))

      const actionButtons = resultTableBodyRows
        .at(0)
        .findAll("._operate .action-bar__table-bar .action-bar__table-button")

      expect(actionButtons.length).to.eq(2)
      expect(actionButtons.at(0).text()).to.eq("編輯")
      expect(actionButtons.at(1).text()).to.eq("刪除")
    })
  })
})
