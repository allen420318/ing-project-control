import { shallowMount, createLocalVue } from "@vue/test-utils"
import { expect } from "chai"
import Vuex from "vuex"
import StreamManage from "../../../src/views/projects/real/StreamManage.vue"
import CasinoLocationSelect from "../../../src/components/selects/CasinoLocation.vue"

const localVue = createLocalVue()
localVue.use(Vuex)

describe("Stream Manage Page", () => {
  let store = {}
  let wrapper = {}
  const $route = { path: "/main/StreamManage.html" }

  beforeEach(() => {
    store = new Vuex.Store()
    wrapper = shallowMount(StreamManage, {
      store,
      localVue,
      mocks: {
        $route
      }
    })
  })

  it("has correct breadcrumbs", () => {
    expect(wrapper.vm.navbarItems.length).to.eq(2)
    expect(wrapper.vm.navbarItems[0].title).to.eq("網管管理")
    expect(wrapper.vm.navbarItems[1].title).to.eq("視訊管理")
  })

  it("has correct title text", () => {
    expect(wrapper.find(".header__title").text()).to.eq("視訊管理")
  })

  it("has casino location select text", () => {
    expect(wrapper.find(".content .action-bar .action-bar__label").text()).to.eq("現場位置 :")
  })

  it("has casino location selector", () => {
    const target = wrapper.find({ ref: "casinoLocationSelect" })
    expect(target.isVisible()).to.be.true
    expect(target.is(CasinoLocationSelect)).to.be.true
  })

  it("will active first tab content", () => {
    const target = wrapper.find(".switch-tab .switch-tab__menu--is-active")
    expect(target.isVisible()).to.be.true
    expect(target.text()).to.eq("CDN管理")
  })

  it("has 7 menu tab", () => {
    const menu = wrapper.findAll(".switch-tab__menu")
    expect(menu.length).to.eq(7)
  })

  it("will switch tab content if clicked tab menu", async () => {
    const menu = wrapper.findAll(".switch-tab__menu")

    menu.at(1).trigger("click")
    await wrapper.vm.$nextTick()
    expect(wrapper.find(".switch-tab__menu--is-active").text()).to.eq("視訊解析度管理")
  })
})
