import { shallowMount } from "@vue/test-utils"
import { expect } from "chai"
import sinon from "sinon"
import DateRange from "../../../src/components/switches/DateRange.vue"

describe("DateRange switch component", () => {
  const fullDateProps = {
    domIdMap: {
      TODAY: "todayId",
      YESTERDAY: "yesterdayId",
      THIS_WEEK: "thisWeekId",
      LAST_WEEK: "lastWeekId",
      THIS_MONTH: "thisMonthId",
      LAST_MONTH: "lastMonthId"
    },
    timeMenu: [
      { code: "TODAY", label: "today" },
      { code: "YESTERDAY", label: "yesterday" },
      { code: "THIS_WEEK", label: "this week" },
      { code: "LAST_WEEK", label: "last week" },
      { code: "THIS_MONTH", label: "this month" },
      { code: "LAST_MONTH", label: "last month" }
    ]
  }

  afterEach(() => {
    sinon.restore()
  })

  it("has correct css classes", () => {
    const wrapper = shallowMount(DateRange, { propsData: { domIdMap: {} } })

    expect(wrapper.findAll(".time-switch-menu .time-switch-menu__item").length).to.eq(4)
  })

  it("will render buttons base on timeMenu props", () => {
    const wrapper = shallowMount(DateRange, {
      propsData: {
        domIdMap: {},
        timeMenu: [
          { code: "TODAY", label: "today" },
          { code: "YESTERDAY", label: "yesterday" }
        ]
      }
    })

    const dateRangeSwitch = wrapper.find(".time-switch-menu")

    expect(dateRangeSwitch.findAll(".time-switch-menu__item").length).to.eq(2)
  })

  it("will assign id base on domIdMap props", () => {
    const wrapper = shallowMount(DateRange, {
      propsData: {
        ...fullDateProps
      }
    })

    const dateRangeSwitch = wrapper.find(".time-switch-menu")

    expect(dateRangeSwitch.findAll(".time-switch-menu__item").length).to.eq(6)

    Object.values(fullDateProps.domIdMap).forEach((x, index) => {
      expect(dateRangeSwitch.find(`.time-switch-menu__item#${x}`).text()).to.eq(fullDateProps.timeMenu[index].label)
    })
  })

  it("will active first option as default button", async () => {
    const wrapper = shallowMount(DateRange, {
      propsData: {
        domIdMap: {},
        timeMenu: [
          { code: "TODAY", label: "today" },
          { code: "YESTERDAY", label: "yesterday" }
        ]
      }
    })

    await wrapper.vm.$nextTick()
    const dateRangeSwitch = wrapper.find(".time-switch-menu")

    expect(dateRangeSwitch.vm.current.timeCode).to.eq("TODAY")
    expect(dateRangeSwitch.find(".time-switch-menu__item--active").text()).to.eq("today")
  })

  it("will switch active button after click", async () => {
    const wrapper = shallowMount(DateRange, {
      propsData: {
        domIdMap: { YESTERDAY: "yesterdayId" },
        timeMenu: [
          { code: "TODAY", label: "today" },
          { code: "YESTERDAY", label: "yesterday" }
        ]
      }
    })

    const yesterdayButton = wrapper.find(".time-switch-menu #yesterdayId")
    yesterdayButton.trigger("click")

    await wrapper.vm.$nextTick()

    expect(wrapper.vm.current.timeCode).to.eq("YESTERDAY")
    expect(yesterdayButton.classes("time-switch-menu__item--active")).to.be.true
  })

  it("can return time of today with start and current range", async () => {
    const today = new Date("2020-01-01T08:30:05")
    sinon.useFakeTimers(today)

    const wrapper = shallowMount(DateRange, {
      propsData: {
        domIdMap: { TODAY: "todayId" },
        timeMenu: [{ code: "TODAY", label: "today" }]
      }
    })

    expect(wrapper.vm.current.timeCode).to.eq("TODAY")
    expect(wrapper.emitted().changeDateRangeOfStartDay[0][0].getTime()).to.eq(new Date("2020-01-01T00:00:00").getTime())
    expect(wrapper.emitted().changeDateRangeOfEndDay[0][0].getTime()).to.eq(new Date("2020-01-01T08:30:05").getTime())
  })

  it("can return time of yesterday with start and end range", async () => {
    const today = new Date("2020-01-01T08:30:05")
    sinon.useFakeTimers(today)

    const wrapper = shallowMount(DateRange, {
      propsData: {
        domIdMap: { YESTERDAY: "yesterdayId" },
        timeMenu: [{ code: "YESTERDAY", label: "yesterday" }]
      }
    })

    expect(wrapper.vm.current.timeCode).to.eq("YESTERDAY")
    expect(wrapper.emitted().changeDateRangeOfStartDay[0][0].getTime()).to.eq(new Date("2019-12-31T00:00:00").getTime())
    expect(wrapper.emitted().changeDateRangeOfEndDay[0][0].getTime()).to.eq(new Date("2020-01-01T00:00:00").getTime())
  })
})
