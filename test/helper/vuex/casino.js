import loginToken from "./loginToken.js"

const result = Object.assign(
  {},
  {
    modules: {
      member: loginToken.modules.member,
      casino: {
        namespaced: true,
        state: { casinoLocations: [] },
        actions: {
          fetchCasinoLocations: () => {
            return {
              exitCode: true,
              version: "0.0.1",
              data: {
                casinoLocations: [
                  {
                    uid: "4a99bdf073cc4ee28fb4bcdc63e2d02c",
                    gameVendorUid: "7d6730d0eee04e12a9e69084acf78b72",
                    gameVendorTitle: "H5",
                    alias: "H5_032",
                    title: "\u5929\u7a7a\u57ce",
                    code: "H5_032",
                    deleteDate: null,
                    lockDate: null,
                    address: "http://fdsfsfsfdsfs",
                    appName: "testhjghj",
                    format: "tests"
                  },
                  {
                    uid: "1f265768df0e4631968402cfaab95a2c",
                    gameVendorUid: "8accd7c2191f4ec48babb45e6f8f571f",
                    gameVendorTitle: "PC",
                    alias: "LOS_SAN",
                    title: "\u6d1b\u8056\u90fd",
                    code: "LOS_SAN",
                    deleteDate: null,
                    lockDate: null,
                    address: null,
                    appName: null,
                    format: null
                  }
                ]
              }
            }
          }
        }
      }
    }
  }
)

export default result
