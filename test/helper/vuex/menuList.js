const getters = {
  getMenuList: () => [
    { key: "代理管理", subMenu: [{ apiName: "agent", key: "最高代理管理" }] },
    {
      key: "基本設定",
      subMenu: [
        { apiName: "tableSpecies", key: "桌種設定" },
        { apiName: "gameSets", key: "遊戲設定" }
      ]
    },
    {
      key: "遊戲管理",
      subMenu: [
        { apiName: "basic", key: "基本項目" },
        { apiName: "themes", key: "廳館管理" },
        { apiName: "tableSets", key: "賭桌管理" },
        { apiName: "gameResult", key: "遊戲紀錄" },
        { apiName: "redLimits", key: "遊戲限紅" }
      ]
    },
    {
      key: "報表功能",
      subMenu: [
        { apiName: "transferRecord", key: "轉帳紀錄" },
        { apiName: "accountingStatistics", key: "帳務統計" },
        { apiName: "playerOrder", key: "會員訂單" },
        { apiName: "abnormal", key: "結算異常查詢" },
        { apiName: "accountingVariance", key: "帳變報表" }
      ]
    },
    {
      key: "系統管理",
      subMenu: [
        { apiName: "permission", key: "角色權限管理" },
        { apiName: "controller", key: "後台使用者管理" }
      ]
    },
    {
      key: "維護管理",
      subMenu: [
        { apiName: "maintain", key: "系統維護" },
        { apiName: "maintainAgent", key: "代理維護" }
      ]
    }
  ]
}

export default {
  modules: {
    menu: { namespaced: true, getters }
  }
}
