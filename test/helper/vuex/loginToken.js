const state = { loginToken: "testLoginToken" }

export default {
  modules: {
    member: {
      namespaced: true,
      state
    }
  }
}
