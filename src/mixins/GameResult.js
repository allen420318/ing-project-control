export const GameResultMixin = {
  methods: {
    parseGameResult(data, gameType) {
      if (gameType === "BACCARAT") {
        const labels = new Map([[1, "莊"], [2, "閒"], [3, "和"]])
        const playerPoint = data[6]
        const bankerPoint = data[7]
        const winner = labels.get(data[8])

        return `${winner} ( 閒${playerPoint} 莊${bankerPoint} )`
      } else if (gameType === "DRAGONTIGER") {
        const labels = new Map([[1, "龍"], [2, "虎"], [3, "和"]])
        const dragonPoints = data[2]
        const tigerPoints = data[3]
        const winner = labels.get(data[4])

        return `${winner} ( 龍${dragonPoints} 虎${tigerPoints} )`
      } else if (gameType === "SICBO") {
        return data.slice(0, -1).join(",")
      } else {
        return data[0]
      }
    }
  }
}

export default GameResultMixin
