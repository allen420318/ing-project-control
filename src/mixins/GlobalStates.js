import store from "../store/index.js"

/* global __BuildVar__ */

export const GlobalStatesMixin = {
  methods: {
    saveGlobalVariablesToVuex() {
      console.log("🚂 Save project state to Vuex")

      const { meta, menu } = __BuildVar__

      store.dispatch("project/setProjectTitle", { key: meta.siteEnvKey, separate: "::" })
      store.dispatch("project/setAPISettings", { ...meta.apiSettings })
      store.dispatch("menu/setMenuList", menu)
    }
  },
  created() {
    this.saveGlobalVariablesToVuex()
  }
}

export default GlobalStatesMixin
