export const computed = {
  computed: {
    isCacheSearchConditionsExist() {
      if (Object.keys(this.searchConditionCache).length > 0) {
        return true
      } else {
        return false
      }
    }
  }
}

export const methods = {
  methods: {
    getCurrencyLabel(currencyId) {
      const targetId = this.isCacheSearchConditionsExist === true ? this.searchConditionCache.currencyId : currencyId
      const [target] = this.currencies.filter(x => x.currencyId === targetId)

      if (target) {
        const { name, code } = target

        return `${name} (${code})`
      } else {
        return ""
      }
    },
    getFetchListPayload({ isFromCacheConditions = false } = {}) {
      if (isFromCacheConditions === true && this.isCacheSearchConditionsExist === true) {
        return {
          ...this.searchConditionCache
        }
      } else {
        return {
          ...this.current
        }
      }
    },
    async searchWithPagingIndex(index) {
      const pagingIndex = parseInt(index)
      this.current.pagingIndex = pagingIndex

      if (this.isCacheSearchConditionsExist === true) {
        const cacheData = this.searchConditionCache

        const payload = Object.assign({}, { ...cacheData }, { pagingIndex: pagingIndex })

        this.saveCacheSearchConditions(payload)
      }

      await this.fetchSearchResult({ isSearchWithCacheConditions: true })
    }
  }
}

export default {
  ...computed,
  ...methods
}
