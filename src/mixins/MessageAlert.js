import { MessageBox } from "element-ui"

export const MessageMixin = {
  methods: {
    showMessage(
      msg,
      {
        customClass = "notice-modal",
        closeOnClickModal = false,
        isShowClose = false,
        isShowCancel = false,
        isShowConfirm = true,
        cancelTextLabel = "取消",
        confirmTextLabel = "確認"
      } = {},
      callback
    ) {
      MessageBox.alert(msg, {
        closeOnClickModal: closeOnClickModal,
        showClose: isShowClose,
        customClass: customClass,
        showCancelButton: isShowCancel,
        cancelButtonText: cancelTextLabel,
        showConfirmButton: isShowConfirm,
        confirmButtonText: confirmTextLabel,
        closeOnPressEscape: false,
        center: true,
        callback: callback
      })
    }
  }
}

export default MessageMixin
