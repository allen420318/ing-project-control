import store from "@/store/index.js"
import ErrorMessageMixin from "@/mixins/ErrorMessage.js"

const generateConfig = payload => {
  let result = {}

  const defaultConfig = {
    method: "POST",
    headers: store.state.project.api.headers
  }

  if (payload) {
    result = Object.assign(defaultConfig, { body: JSON.stringify(payload) })
  }

  return result
}

export const fetcher = async payload => {
  const endpoint = payload.endpoint ? payload.endpoint : store.state.project.api.endpoint
  const config = generateConfig(payload)
  const response = await fetch(endpoint, config)

  if (response.ok === false) {
    const httpStatus = parseInt(response.status)

    if (httpStatus === 403) {
      const permissionErrorMessage = { message: "已無權限，請重整[確定]" }
      const redirectToIndexPage = () => window.location.replace("/")
      ErrorMessageMixin.methods.showErrorMessage(permissionErrorMessage, redirectToIndexPage)
    } else {
      throw new Error(`${httpStatus} - API 回應錯誤`)
    }
  } else {
    return response.json()
  }
}

export default fetcher
