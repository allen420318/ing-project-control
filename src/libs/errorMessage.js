const Chinese = [
  //Login 登入相關
  {
    sub: "VerifyPassword",
    msg: "密碼輸入錯誤"
  },
  {
    sub: "AccountIsDisabled",
    msg: "此帳號已停用"
  },
  {
    sub: "AccountIsLocked",
    msg: "帳號已鎖定"
  },
  {
    sub: "AccountNotExists",
    msg: "無此帳號"
  },
  {
    sub: "NotExistByGroup",
    msg: "最高代理(群組)不存在"
  },
  {
    sub: "ForceChangePassword",
    msg: "強制變更密碼"
  },
  {
    sub: "LoginTokenInvalid",
    msg: "loginToken已失效"
  },
  {
    sub: "PasswordDuplicateFromPast",
    msg: "新密碼不可與舊密碼相同"
  },
  {
    sub: "AccountFormatError",
    msg: "帳號格式錯誤"
  },
  {
    sub: "PasswordFormatError",
    msg: "密碼格式錯誤"
  },
  {
    sub: "PasswordFirstError",
    msg: "密碼第一次錯誤"
  },
  {
    sub: "PasswordSecondError",
    msg: "密碼第二次錯誤"
  },
  {
    sub: "SubAgentIsDisabled",
    msg: "子帳號已被停用"
  },
  {
    sub: "AgentWhiteListIPNotInList",
    msg: "請求ip地址不合法"
  },
  {
    sub: "AgentWhiteListIPAlreadyExist",
    msg: "代理端白名單ip已存在"
  },
  {
    sub: "AgentWhiteListIPCountOverLimit",
    msg: "代理端白名單ip超過數量限制"
  },
  {
    sub: "GameApiWhiteListIpExist",
    msg: "GameApi -白名單IP已存在"
  },
  //Table 資料表相關
  {
    sub: "RecordDefaultOption",
    msg: "資料已是預設選項"
  },
  {
    sub: "RecordDeleted",
    msg: "資料已刪除"
  },
  {
    sub: "RecordLocked",
    msg: "資料已上鎖"
  },
  {
    sub: "RecordNotChanged",
    msg: "資料沒有異動"
  },
  {
    sub: "RecordNotExists",
    msg: "資料不存在"
  },
  {
    sub: "RecordNotModify",
    msg: "資料不可編輯"
  },
  {
    sub: "RecordNotDelete",
    msg: "資料不可刪除"
  },
  {
    sub: "RecordRefuse",
    msg: "資料不可操作"
  },
  {
    sub: "RequiredRecordNotExists",
    msg: "必要欄位不存在"
  },
  {
    sub: "RecordUnLocked",
    msg: "資料未遭上鎖"
  },
  {
    sub: "DefaultOptionNotAllowedDelete",
    msg: "預設資料不允許刪除"
  },
  {
    sub: "DefaultOptionNotAllowedLock",
    msg: "預設資料不允許鎖定"
  },
  {
    sub: "IntegrityConstraintViolation",
    msg: "資料完整性錯誤"
  },
  {
    sub: "DataEmpty",
    msg: "資料為空值"
  },
  {
    sub: "ReportDoesNotExist",
    msg: "報表不存在"
  },
  {
    sub: "IndexCrossPage",
    msg: "指定的索引超過最大頁數"
  },
  {
    sub: "RecordCanNotRead",
    msg: "資料不允許瀏覽"
  },
  {
    sub: "RecordIsRepeat",
    msg: "資料已重複"
  },
  {
    sub: "RecordExist",
    msg: "資料已存在"
  },
  {
    sub: "RecordIsEqual",
    msg: "輸入資料與原資料相同"
  },
  {
    sub: "FormatErrorByDate",
    msg: "開始時間不可在當前時間之前"
  },
  {
    sub: "FormatError",
    msg: "格式錯誤"
  },
  {
    sub: "RecordIsRealationOther",
    msg: "資料有關聯到其他地方，不允許做其他操作"
  },
  {
    sub: "RecordBatchErrorByDelete",
    msg: "資料批次刪除錯誤"
  },
  {
    sub: "RecordBatchErrorByLock",
    msg: "資料批次鎖定錯誤"
  },
  {
    sub: "RecordBatchErrorByUnLock",
    msg: "資料批次解鎖錯誤"
  },
  {
    sub: "RecordSaveFail",
    msg: "存取失敗"
  },
  {
    sub: "TableIsOpen",
    msg: "桌種已開放"
  },
  {
    sub: "SiteCodeIsRepeat",
    msg: "現場桌號已重複"
  },
  {
    sub: "FrontCodeIsRepeat",
    msg: "前台桌號已重複"
  },
  //System 系統
  {
    sub: "ServiceNotExists",
    msg: "服務不存在"
  },
  {
    sub: "DataNoVerified",
    msg: "資料未經過驗證"
  },
  {
    sub: "EventNotExists",
    msg: "事件不存在"
  },
  {
    sub: "MustBeUseFunc",
    msg: "必須使一個函式或方法"
  },
  {
    sub: "TypeAliasIsNotExist",
    msg: "全域設定類型別名不存在"
  },
  {
    sub: "TokenNotExists",
    msg: "Token不存在"
  },
  {
    sub: "PermissionNotExists",
    msg: "無此權限"
  },
  {
    sub: "NotYetStartMaintain",
    msg: "請先進入全站維護才可執行次動作"
  },
  {
    sub: "NotYetStartAgentMaintain",
    msg: "請先進入代理維護才可執行次動作"
	},
	{
    sub: "NotYetStartTableMaintain",
    msg: "請先進入賭桌維護才可執行次動作"
  },
  {
    sub: "FinishedMaintain",
    msg: "維護已結束"
  },
  {
    sub: "StartMaintain",
    msg: "維護正在執行"
  },
  {
    sub: "ResetMaintain",
    msg: "維護重啟中"
  },
  {
    sub: "CanNotResetMaintain",
    msg: "維護不可重啟"
  },
  {
    sub: "MaintainExisted",
    msg: "維護已存在"
  },
  {
    sub: "MaintainNotice",
    msg: "已通知維護即將開始"
  },
  {
    sub: "MaintainNotNotice",
    msg: "維護非已通知狀態"
  },
  {
    sub: "CaptchaFail",
    msg: "驗證碼驗證失敗"
  },
  {
    sub: "CaptchaExpired",
    msg: "驗證碼已過期"
  },
  {
    sub: "UserIsNotTopAgent",
    msg: "使用者不是最高代理"
  },
  {
    sub: "DataInValid",
    msg: "資料格式錯誤"
  },
  //Permission 權限
	{
    sub: "RoleNameIsBeenUsed",
    msg: "角色名稱已被使用"
  },
  {
    sub: "RoleIsBindSubAgent",
    msg: "已套用使用者，無法刪除"
  },
  //Account 帳號維護相關
  {
    sub: "AgentGroupIsDisabled",
    msg: "此帳號已停用"
  },
  {
    sub: "AgentKeyIsDisabled",
    msg: "代理金鑰已被停用"
  },
  {
    sub: "AgentCodeHasBeenUsed",
    msg: "代理識別碼不能使用"
  },
  {
    sub: "UserIsNotExist",
    msg: "使用者不存在"
  },
  {
    sub: "MailboxHasBeenUsed",
    msg: "信箱已被使用"
  },
  {
    sub: "UserAccountHasBeenUsed",
    msg: "帳號已被使用"
  },
  {
    sub: "DoNotDisabledSelf",
    msg: "無法停用自己"
  },
  {
    sub: "DoNotDeleteSelf",
    msg: "無法刪除自己"
  },
  {
    sub: "CurrencyIsExist",
    msg: "貨幣已存在"
  },
  {
    sub: "CurrencyUseTaiwan",
    msg: "貨幣使用台幣"
  },
  {
    sub: "UserIsDisabled",
    msg: "已為停用狀態"
  },
  {
    sub: "UserIsLock",
    msg: "已為上鎖狀態"
  },
  {
    sub: "UserIsEnabled",
    msg: "已為啟用狀態"
  },
  {
    sub: "UserIsNotTopAgent",
    msg: "使用者不是最高代理"
  },
  {
    sub: "NotUsedTaiwanCountryCode",
    msg: "國碼不能使用886"
  },
  {
    sub: "CurrencyUnableDelete",
    msg: "已套用代理，無法刪除"
  },
  {
    sub: "CurrencyLastOne",
    msg: "貨幣剩餘一個，無法刪除"
  },
  {
    sub: "LanguageSetCodeNotSet",
    msg: "未設定進入遊戲預設語系"
  },
  {
    sub: "invalidLanguageSetCode",
    msg: "非法的遊戲預設語系"
  },
  {
    sub: "AgentCodeIsRepeat",
    msg: "代理識別代碼重複"
  },
  //Game 遊戲相關
  {
    sub: "NotAllowedRefunded",
    msg: "不允許退押注"
  },
  {
    sub: "GameHasBeenUsed",
    msg: "遊戲已被使用, 不允許此操作"
  },
  {
    sub: "NoModificationAllowed",
    msg: "不允許修改賽果"
  },
  {
    sub: "ChannelIdIsRepeat",
    msg: "CDN頻道重複使用"
  },
  {
    sub: "ReloadTableConfigError",
    msg: "[Server] 重載賭桌失敗"
  }
]

module.exports = {
  textMessage: Chinese
}

/* export  router; */
