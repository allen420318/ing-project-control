const CARD_SUITS = new Map([
  ["S", "Spades"],
  ["H", "Hearts"],
  ["D", "Diamonds"],
  ["C", "Clubs"]
])

export const CARD_NUMBERS = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "J", "Q", "K"]

export const getFullCardList = () => {
  const suits = ["S", "H", "D", "C"]
  const result = suits.map(s => CARD_NUMBERS.flatMap(n => `${s}${n}`))
  return result
}

export const parseGameResult = dataArray => {
  const cardSuitCode = dataArray[0]
  const cardValueCode = dataArray[dataArray.length - 1]
  const cardSuit = getCardSuitFromCode(cardSuitCode)
  const cardValue = getCardValueFromCode(cardValueCode)

  return { suitCode: cardSuitCode, suit: cardSuit, valueCode: cardValueCode, value: cardValue, originData: dataArray }
}

export const mappingCardItems = dataArrayItem => {
  const { suitCode, value, originData } = parseGameResult(dataArrayItem)
  return { suitClass: suitCode, value, originData }
}

export const getCardValueFromCode = code => {
  const result = parseInt(code)
  switch (result) {
    case 0:
      return "10"
    case 1:
      return "A"
    default:
      return code
  }
}

export const getCardSuitFromCode = suitCode => {
  return CARD_SUITS.get(suitCode)
}
