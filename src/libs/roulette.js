const factorySectionColorMap = (list, color) => {
  const result = new Map()
  const value = color

  list.forEach(x => {
    result.set(x, value)
  })

  return result
}

const greenSections = [0]
const redSections = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36]
const blackSections = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35]

const wheelSectionColorMap = new Map([
  ...factorySectionColorMap(redSections, "red"),
  ...factorySectionColorMap(blackSections, "black"),
  ...factorySectionColorMap(greenSections, "green")
])

export const getSectionColorMap = () => {
  return wheelSectionColorMap
}

export const getSectionColor = sectionNumber => {
  return wheelSectionColorMap.get(sectionNumber)
}

export const getFullRouletteList = () => {
  const list = [...Array(37).keys()].map(x => x + 1)
  return [...list, 0].map(x => {
    return { value: x, color: getSectionColor(x) }
  })
}

export default { getSectionColorMap, getSectionColor, getFullRouletteList }
