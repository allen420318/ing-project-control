export const randomPassword = () => {
  const hash =
    Math.random()
      .toString(36)
      .slice(2) +
    Math.random()
      .toString(36)
      .slice(2)

  const randomDigit = Math.floor(1 + Math.random() * 9)
  const replaceDigit = Math.floor(1 + Math.random() * 9)

  return hash.slice(0, 10).replace(/o|O|0/g, replaceDigit) + randomDigit
}
