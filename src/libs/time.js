export const getPreviousDateByDays = (previousDays, specificDate = new Date()) => {
  const result = new Date(specificDate.getTime())
  result.setDate(result.getDate() - previousDays)
  return result
}

export const getPreviousDateByMonthes = (previousMonths, specificDate = new Date()) => {
  const result = new Date(specificDate.getTime())
  result.setMonth(result.getMonth() - previousMonths)
  return result
}

export const getFirstDateOfThisWeek = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  return new Date(target.setDate(target.getDate() - target.getDay()))
}

export const getLastDateOfThisWeek = (specificDate = new Date()) => {
  const firstDate = getFirstDateOfThisWeek(specificDate)
  const target = new Date(specificDate)
  return new Date(target.setDate(firstDate.getDate() + 6))
}

export const getFirstDateOfLastWeek = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  const diff = target.getDay() + 7
  return new Date(target.setDate(target.getDate() - diff))
}

export const getLastDateOfLastWeek = (specificDate = new Date()) => {
  const date = getFirstDateOfLastWeek(specificDate)
  const diffDaysWithFirstDate = date.getDate() - date.getDay() + 6
  date.setDate(diffDaysWithFirstDate)
  return date
}

export const getFirstDateOfThisMonth = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  return new Date(target.getFullYear(), target.getMonth(), 1)
}

export const getFirstDateOfLastMonth = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  return new Date(target.getFullYear(), target.getMonth() - 1, 1)
}

export const getLastDateOfLastMonth = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  target.setDate(0)
  return target
}

export const getDateWithStartTime = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  target.setHours(0, 0, 0, 0)
  return target
}

export const getDateWithEndTime = (specificDate = new Date()) => {
  const target = new Date(specificDate)
  target.setHours(23, 59, 59, 999)
  return target
}

export const formatToUTCTimeString = timeString => {
  const target = timeString.split(" ")
  return `${target[0]}T${target[1]}Z`
}

export const getLocalTimeString = (specificDate = new Date()) => {
  const target = new Date(specificDate)

  const date = [target.getFullYear(), `${target.getMonth() + 1}`.padStart(2, 0), `${target.getDate()}`.padStart(2, 0)]
  const time = [
    `${target.getHours()}`.padStart(2, 0),
    `${target.getMinutes()}`.padStart(2, 0),
    `${target.getSeconds()}`.padStart(2, 0)
  ]
  return `${date.join("/")} ${time.join(":")}`
}

export const getAPIFormatTimeString = (specificDate = new Date()) => {
  const target = new Date(specificDate)

  const date = [
    target.getUTCFullYear(),
    `${target.getUTCMonth() + 1}`.padStart(2, 0),
    `${target.getUTCDate()}`.padStart(2, 0)
  ]
  const time = [
    `${target.getUTCHours()}`.padStart(2, 0),
    `${target.getUTCMinutes()}`.padStart(2, 0),
    `${target.getUTCSeconds()}`.padStart(2, 0)
  ]
  return `${date.join("/")} ${time.join(":")}`
}

export const getDateString = (specificDate = new Date(), isAddLeadingZero = true) => {
  const target = new Date(specificDate)
  if (isAddLeadingZero === true) {
    const date = [target.getFullYear(), `${target.getMonth() + 1}`.padStart(2, 0), `${target.getDate()}`.padStart(2, 0)]
    return date.join("-")
  } else {
    const date = [target.getFullYear(), `${target.getMonth() + 1}`, target.getDate()]
    return date.join("-")
  }
}

export const getDaysBetweenDates = (start, end) => {
  const oneDay = 1000 * 60 * 60 * 24
  const startDate = new Date(start)
  const endDate = new Date(end)

  return (startDate.getTime() - endDate.getTime()) / oneDay
}

export const getWithinDaysDate = (specificDate = new Date(), limitDays = 7, limitDate = new Date()) => {
  const target = new Date(specificDate)
  const limitTarget = new Date(limitDate)

  target.setDate(target.getDate() + limitDays)

  return target.getTime() >= limitDate.getTime() ? limitTarget : target
}
