class common {
  constructor() {
    this._cards = []
  }

  timeZoneApply(_time, toUtc) {
    var locales = "zh-CN"
    var dateOptions = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
      hour12: false
    }

    var d = new Date(_time)
    //var h = d.getTimezoneOffset()/60;
    var m = d.getTimezoneOffset()
    var multiplier = toUtc ? 1 : -1
    //d.setTime(d.getTime() + (h*60*60*1000)*multiplier);
    d.setTime(d.getTime() + m * 60 * 1000 * multiplier)
    return d.toLocaleString(locales, dateOptions)
  }

  LimitNowTme(_time) {
    var d = new Date(_time)
    var d2 = new Date()
    let a1 = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate()
    let a2 = d2.getFullYear() + "-" + d2.getMonth() + "-" + d2.getDate()
    var toleranceScope = 10
    if (a1 == a2) {
      d2.setTime(d2.getTime() - toleranceScope * 1000)
      return d2
    } else {
      return _time
    }
  }

  setDay(item) {
    var today = new Date(),
      start = new Date(),
      end = new Date(),
      oneDayTime = 86400,
      re = {}
    switch (item) {
      case "today":
        start = today
        end = today
        break

      case "week":
        var w = today.getDay()
        start.setTime(today.getTime() - oneDayTime * 1000 * w)
        end.setTime(today.getTime() + oneDayTime * 1000 * (6 - w))

        break

      case "month":
        var year = today.getFullYear()
        var month = today.getMonth()
        var ta = new Date(year, month)
        var ta2 = new Date(year, month + 1)
        start.setTime(ta.getTime())
        end.setTime(ta2.getTime() - oneDayTime)

        break

      case "LastMonth":
        var year = today.getFullYear()
        var month = today.getMonth()
        var ta = new Date(year, month - 1)
        var ta2 = new Date(year, month)
        start.setTime(ta.getTime())
        end.setTime(ta2.getTime() - oneDayTime)

        break
    }

    var locales = "zh-CN"
    var dateOptions = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    }

    start =
      start
        .toLocaleString(locales, dateOptions)
        .slice(0, 10)
        .replace(/\//g, "-") + " 00:00:00"
    end =
      end
        .toLocaleString(locales, dateOptions)
        .slice(0, 10)
        .replace(/\//g, "-") + " 23:59:59"

    re = {
      start: start,
      end: end
    }

    //console.log(re);

    return re
  }

  setDayV2(key) {
    let start = new Date()
    let end = new Date()

    switch (key) {
      case "today":
        break

      case "week":
        start = new Date(start.setDate(start.getDate() - start.getDay()))
        break

      case "month":
        start = new Date(start.getFullYear(), start.getMonth(), 1)
        break

      case "LastMonth":
        start = new Date(start.getFullYear(), start.getMonth() - 1, 1)
        end.setDate(0)
        break

      case "YESTERDAY":
        start.setDate(start.getDate() - 1)
        end.setDate(end.getDate() - 1)
        break

      case "THIS_WEEK_WITHOUT_TODAY":
        start = new Date(start.setDate(start.getDate() - start.getDay()))
        end.setDate(end.getDate() - 1)
        break

      case "THIS_MONTH_WITHOUT_TODAY":
        start = new Date(start.getFullYear(), start.getMonth(), 1)
        end.setDate(end.getDate() - 1)
        break
    }

    const locales = "zh-CN"
    const dateOptions = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    }

    const startDate =
      start
        .toLocaleString(locales, dateOptions)
        .slice(0, 10)
        .replace(/\//g, "-") + " 00:00:00"

    const endDate =
      end
        .toLocaleString(locales, dateOptions)
        .slice(0, 10)
        .replace(/\//g, "-") + " 23:59:59"

    const result = {
      start: startDate,
      end: endDate
    }

    return result
  }

  diffDay(Nowdate, DiffMonth) {
    var _now = new Date(Nowdate)
    var _year = _now.getFullYear(),
      _month = _now.getMonth(),
      _day = _now.getDate(),
      days = 0,
      re = "",
      a,
      month = 0
    var _nowDays = new Date(_year, _month + 1, 0).getDate()
    month = _month + DiffMonth + 1
    var _ta = new Date(_year, month, 0)
    var _taDays = _ta.getDate()

    switch (true) {
      case _day == _nowDays:
        days = 0
        break

      default:
        month = month - 1
        days = _day
        break
    }

    a = new Date(_year, month, days)

    var locales = "zh-CN"
    var dateOptions = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    }

    re = a
      .toLocaleString(locales, dateOptions)
      .slice(0, 10)
      .replace(/\//g, "-")
    //console.log(re) ;
    return re
  }

  cards() {
    var suit = ["S", "H", "D", "C"]
    var re = []
    _.forEach(suit, function(alias, k) {
      var num = 13
      var t = { alias: alias, Boards: [] }

      while (num--) {
        var val = t.Boards.length + 1
        var title = val

        switch (val) {
          case 1:
            title = "A"
            break

          case 10:
            val = "0"
            title = "10"
            break

          case 11:
            val = "J"
            title = "J"
            break

          case 12:
            val = "Q"
            title = "Q"
            break

          case 13:
            val = "K"
            title = "K"
            break
        }

        t.Boards.push({ suit: alias, title: title, val: alias + "" + val })
      }
      re.push(t)
    })

    re.push({ alias: "N", Boards: [{ suit: "N", title: "", val: "" }] })
    return re
  }

  getCard(val) {
    if (this._cards.length == 0) {
      this._cards = this.cards()
    }
    var cards = _.cloneDeep(this._cards)
    var re = {},
      Boards = []
    _.forEach(cards, function(v, k) {
      Boards = Boards.concat(v.Boards)
      //Boards = Object.assign(Boards,v.Boards);
    })

    var re = _.filter(Boards, { val: val })
    return re
  }
}

export default new common()
