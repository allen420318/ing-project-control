Vue.config.devtools = true

/*----------------------------
	全站所需的css
------------------------------*/

import "normalize.css"
import "@/styles/less/common.less"

/*----------------------------
	引入vue和根组件app.vue
------------------------------*/
import Vue from "vue"
import App from "@/app.vue"
import VueMeta from "vue-meta"

/*----------------------------
	資料處理的tool
------------------------------*/
import common from "@/libs/common"
import { charFillUp } from "@/libs/string.js"

// Will remove in the future
import _ from "lodash"
import GlobalStatesMixin from "./mixins/GlobalStates.js"
import ErrorMessageMixin from "./mixins/ErrorMessage.js"
import { checkVersion, getVersionFromUrl } from "./helpers/versionCheck.js"

/*----------------------------
	vuex
	資料儲存
------------------------------*/
import store from "@/store/index.js"

/*----------------------------
	vue router
	router
------------------------------*/
import VueRouter from "vue-router"
import routes from "./routerSwitch.js"
import { applyMenuPermission } from "./helpers/permission.js"
import { checkRoutePermission, checkLoginToken } from "./helpers/routerGuard.js"

Vue.use(VueMeta)
Vue.use(VueRouter)

GlobalStatesMixin.methods.saveGlobalVariablesToVuex()

const router = new VueRouter({
  mode: "history",
  routes
})

router.beforeEach(async (to, from, next) => {
  try {
    // check passed on login page
    const routeTargetName = to.name
    const loginToken = store.state.member.loginToken
    const notCheckLoginRouteNames = ["Login", "ForgetPassword", "ResetPassword", "PageNotFound"]

    await store.dispatch("i18n/loadLanguageFile", { isSwichBaseOnStorage: true, isAlwaysLoadFromCache: true })

    if (notCheckLoginRouteNames.includes(routeTargetName) === false) {
      // Check login token on every applied route
      const loginTokenValidResult = await checkLoginToken(loginToken)

      if (loginTokenValidResult !== true) {
        store.dispatch("member/removeUserLoginData")
        next(false)
        router.app.$destroy()
        window.location.replace("/login.html")
      }

      // Check menu permission & rebuild
      await applyMenuPermission()

      const routePermissionResult = checkRoutePermission(routeTargetName)

      if (routePermissionResult === false) {
        const message = { message: "已無權限，請重整[確定]" }
        ErrorMessageMixin.methods.showErrorMessage(message, () => next({ name: "index" }))

        // Force break , do not go further
        return
      }
    }

    // You are free to go!
    next()
  } catch (err) {
    next(false)

    ErrorMessageMixin.methods.showErrorMessage(err, () => {
      store.dispatch("member/removeUserLoginData")
      router.app.$destroy()
      window.location.replace("/login.html")
    })
  }
})

/*----------------------------
	vee-validate
	驗證表單
------------------------------*/

import VeeValidate, { Validator } from "vee-validate"
Validator.extend("requiredAllowSpace", {
  messages: field => "The " + field + " required",
  validate(value) {
    var regex1 = /^[\w\s]+$/g
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("alpha_underline", {
  messages: field => "The " + field + " required",
  validate(value) {
    var regex1 = /^[a-zA-Z0-9_]+$/g
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("beginWithAlpha", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /^[a-zA-Z]{1}\w*$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("beginWithAlphaV1", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /^[a-zA-Z]{1}[\da-zA-Z]*$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("bigAlphaUnderline", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /^[A-Z_]*$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("bigAlphaUnderlineNum", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /^[A-Z_0-9]*$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("AlphaNumChineseUnderline", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /[\u4e00-\u9fa5\da-zA-Z_]+$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("AlphaNumSymbol", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /^[a-zA-Z_./:0-9-]*$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("AlphaNumChinese", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /[\u4e00-\u9fa5\da-zA-Z]+$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("AlphaNumChineseSymbol", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-./:;<=>?@[\]^_`{|}~\u4e00-\u9fa5\da-zA-Z]+$/gy
    var r = regex1.test(value)

    return r
  }
})

Validator.extend("beginNoZero", {
  messages: field => "The " + field + " format is not correct",
  validate(value) {
    var regex1 = /^([1-9]{1}[0-9]*)$/gy
    var r = regex1.test(value)

    return r
  }
})

Vue.use(VeeValidate, {
  events: "input|blur",
  errorBagName: "V_err",
  fieldsBagName: "V_fields",
  classes: true,
  classNames: {
    valid: "is-valid",
    invalid: "is-invalid"
  }
})

/*----------------------------
	element-ui
	ui 的元件
------------------------------*/

import { Dialog, Button, MessageBox, Loading, pagination, Message, Popover } from "element-ui"
Vue.use(Dialog)
Vue.use(Button)
Vue.use(Loading)
Vue.use(pagination)
Vue.use(Popover)

Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$message = Message
Vue.prototype.$Loading = Loading

/*----------------------------
	vue filter
------------------------------*/

Vue.filter("toMoney", function(value) {
  if (!value) return "0"
  if (typeof value != "number") {
    value = parseFloat(value)
  }

  return value.toLocaleString()
})

Vue.filter("toMoneyV2", function(value, decimalNum) {
  const validFloatNumber = /-?[0-9]*(\.?[0-9]*)$/
  const isValid = validFloatNumber.test(value)

  if (!isValid || value === 0) return "0.00"

  const [integerSection, decimalSection] = value.toString().split(".")

  const decimalValue = charFillUp(decimalSection, decimalNum)

  return `${integerSection}.${decimalValue}`
})

Vue.filter("utc2local", function(value) {
  return common.timeZoneApply(value, false)
})

Vue.filter("gameResulTtext", function(value, gameType, gameTypeAlias) {
  if (value === null) return "-"
  var a = gameTypeAlias
  var re = ""
  var val = value
  switch (gameType) {
    case a.BACCARAT:
      var _result = ["莊", "閒", "和"]
      re = _result[val[8] - 1] + " ( " + _result[1] + val[6] + " " + _result[0] + val[7] + " )"
      break

    case a.DRAGONTIGER:
      var _result2 = ["龍", "虎", "和"]
      re = _result2[val[4] - 1] + " ( " + _result2[0] + val[2] + " " + _result2[1] + val[3] + " )"
      break

    case a.SICBO:
      re = val[0] + "," + val[1] + "," + val[2]
      break

    case a.FANTAN:
      re = val[0]
      break

    case a.ROULETTE:
      re = val[0]
      break
  }
  return re
})

new Vue({
  el: "#application",
  router,
  store,
  render: h => h(App),
  async created() {
    try {
      const versionFileLocation = "/version.txt"
      const result = await getVersionFromUrl(versionFileLocation)
      console.log("version : ", result)
      const isInSameVerion = checkVersion(result)
      isInSameVerion === false ? location.reload(true) : ""
    } catch (err) {
      console.log(err)
    }
  }
})
