import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "SystemManage"

export const getGameAPIIPWhiteList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "GameAPIWhiteListIP",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        agentGroupUid: data.agentGroupUid
      }
    }
  })

  return result
}

export const addGameAPIIPWhiteList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "GameAPIWhiteListIP",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        agentGroupUid: data.agentGroupUid,
        ip: data.ip
      }
    }
  })

  return result
}

export const deleteGameAPIIPWhiteList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "GameAPIWhiteListIP",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        agentGroupUid: data.agentGroupUid,
        ip: data.ip
      }
    }
  })

  return result
}

export const getAgentIPWhiteList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AgentWhiteListIP",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        agentGroupUid: data.agentGroupUid
      }
    }
  })

  return result
}

export const addAgentIPWhiteList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AgentWhiteListIP",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        agentGroupUid: data.agentGroupUid,
        ip: data.ip
      }
    }
  })

  return result
}

export const deleteAgentIPWhiteList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AgentWhiteListIP",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        agentGroupUid: data.agentGroupUid,
        ip: data.ip
      }
    }
  })

  return result
}

export default {
  getGameAPIIPWhiteList,
  addGameAPIIPWhiteList,
  deleteGameAPIIPWhiteList,
  getAgentIPWhiteList,
  addAgentIPWhiteList,
  deleteAgentIPWhiteList
}
