import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "agentFeatures"

export const getClientLanguageList = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "LanguageSet",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list"
    }
  })

  return result
}

export default {
  getClientLanguageList
}
