import fetcher from "@/libs/fetcher.js"
import { paginationBuilder } from "./helper.js"

const WORKING_DOMAIN = "agentFeatures"

export const getTopAgentList = async (loginToken, account = "") => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "getTopAgentList",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      account
    }
  })

  return result
}

export const getTopAgenBasicList = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "TopAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "getCurrencyTopAgentList"
    }
  })

  return result
}

export const getTopAgentListByWalletId = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "walletOptions",
      formData: {
        wallet: data.userWalletId ? [data.userWalletId] : []
      }
    }
  })

  return result
}

export const getTopAgentDetailList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "topAgent",
        ...paginationBuilder(data),
        conditions: {
          filters: [],
          where: {
            account: {
              account: data.topAgentAccount,
              currencyId: data.currencyId
            }
          }
        }
      }
    }
  })

  return result
}

export const disableTopAgent = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "disable",
      password: data.userPassword,
      formData: {
        uid: data.topAgentUid
      }
    }
  })

  return result
}

export const enableTopAgent = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "enable",
      password: data.userPassword,
      formData: {
        uid: data.topAgentUid
      }
    }
  })

  return result
}

export const unlockTopAgent = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "unlock",
      password: data.userPassword,
      formData: {
        uid: data.topAgentUid
      }
    }
  })

  return result
}

export const editTopAgent = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        uid: data.topAgentUid,
        nickname: data.nickname,
        countryCode: data.phoneCountryCode,
        mobile: data.phoneNumber,
        email: data.email,
        walletId: data.walletTypeId,
        apiUrl: data.apiEndpoint,
        apiSignKey: data.apiSignKey,
        reportUids: data.associateAccounts,
        languageSetCode: data.languageSetCode,
        agentGameUrl: data.agentGameUrl
      }
    }
  })

  return result
}

export const getTopAgentDetail = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "getTopAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      uid: data.topAgentUid
    }
  })

  return result
}

export const addTopAgent = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        account: data.topAgentAccount,
        nickname: data.nickname,
        password: data.password,
        code: data.code,
        countryCode: data.phoneCountryCode,
        mobile: data.phoneNumber,
        email: data.email,
        walletId: data.walletTypeId,
        currencyId: data.currencyId,
        apiUrl: data.apiUrl,
        reportUids: data.reportUids,
        languageSetCode: data.languageSetCode,
        AgentWhiteListIP: data.agentWhiteListIP,
        GameAPIWhiteListIP: data.gameAPIWhiteListIP,
        agentGameUrl: data.agentGameUrl
      }
    }
  })

  return result
}

export const getTopAgentAccountPermissionList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "getReportPermission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      account: data.topAgentAccount
    }
  })

  return result
}

// API KEY

export const getTopAgentAPIKeyData = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "getAgentKey",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      groupUid: data.topAgentGroupUid
    }
  })

  return result
}

export const generateTopAgentAPIKey = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "agentKey",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "generate",
      password: data.userPassword,
      formData: {
        uid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export const generateWalletAPISignKey = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "topAgent",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "randmonSign"
    }
  })

  return result
}

export const enableTopAgentAPIKey = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "agentKey",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "enable",
      password: data.userPassword,
      formData: {
        uid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export const disableTopAgentAPIKey = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "agentKey",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "disable",
      password: data.userPassword,
      formData: {
        uid: data.topAgentGroupUid
      }
    }
  })

  return result
}

export default {
  getTopAgentList,
  getTopAgenBasicList,
  getTopAgentListByWalletId,
  getTopAgentDetailList,
  disableTopAgent,
  enableTopAgent,
  unlockTopAgent,
  editTopAgent,
  getTopAgentDetail,
  getTopAgentAPIKeyData,
  generateTopAgentAPIKey,
  generateWalletAPISignKey,
  enableTopAgentAPIKey,
  disableTopAgentAPIKey,
  addTopAgent,
  getTopAgentAccountPermissionList
}
