export function getArrayFrom(x) {
  return Array.from({ length: x }, (v, k) => k + 1)
}

export function orderStatesBuilder(data) {
  return data.length > 0 ? data : getArrayFrom(4)
}

export function filterBuilder(data) {
  const gameHall = data.gameHall ? [data.gameHall] : []
  const gameType = data.gameType ? [data.gameType] : []
  const agentAccount = data.topAgentAccount ? [data.topAgentAccount] : []
  const gameOrderState = orderStatesBuilder(data.gameOrderStates)

  return { filters: { themes: gameHall, state: gameOrderState, gameSets: gameType, agentGroup: agentAccount } }
}

export function paginationBuilder(data) {
  return { pagination: { pageSize: data.itemsPerPage, pageIndex: data.pagingIndex } }
}
