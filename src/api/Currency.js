import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "agentFeatures"

export const getCurrencyList = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "currency",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list"
    }
  })

  return result
}

export const addCurrency = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "currency",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        country: data.countryName,
        name: data.currencyName,
        code: data.currencyCode
      }
    }
  })

  return result
}

export const deleteCurrency = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "currency",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        id: data.currencyId
      }
    }
  })

  return result
}

export const sortCurrencyOrder = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "currency",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "sort",
      formData: {
        idList: data.currencyIdList
      }
    }
  })

  return result
}

export default {
  getCurrencyList,
  addCurrency,
  deleteCurrency,
  sortCurrencyOrder
}
