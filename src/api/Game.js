import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "gameManagement"

export const getGameHallList = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "basicItems",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      dataTypes: ["themes"]
    }
  })

  return result
}

export const getGameTypeList = async loginToken => {
  const result = await fetcher({
    ns: "Assist",
    work: "basicOptions",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        optionTypes: { gameSets: { modeSet: [] } }
      }
    }
  })

  return result
}

export const getGameStateList = async loginToken => {
  const result = await fetcher({
    ns: "Assist",
    work: "basicOptions",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        optionTypes: { gameResultState: { modeSet: [] } }
      }
    }
  })

  return result
}

export const getGameOrderDetail = async (loginToken, payload) => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "playerOrder",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "serialDetails",
      formData: {
        betNo: payload.orderNumber
      }
    }
  })

  return result
}

export const getGameTransactionTypeList = async loginToken => {
  const result = await fetcher({
    ns: "Assist",
    work: "basicOptions",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        optionTypes: { transferTypes: { modeSet: [] } }
      }
    }
  })

  return result
}

export const getGameTypeWithGameTableList = async loginToken => {
  const result = await fetcher({
    ns: "Basic",
    work: "gameSets",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list"
    }
  })

  return result
}

export const getGameResultDetail = async (loginToken, data) => {
  const result = await fetcher({
    ns: "gameManagement",
    work: "gameResult",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "detail",
      formData: {
        uid: data.gameResultUid
      }
    }
  })

  return result
}

export const modifyGameResult = async (loginToken, data) => {
  const result = await fetcher({
    ns: "gameManagement",
    work: "gameResult",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      password: data.userPassword,
      formData: {
        result: data.gameResult,
        uid: data.gameResultUid,
        reason: data.reasonOfModify
      }
    }
  })

  return result
}

export const returnGameBets = async (loginToken, data) => {
  const result = await fetcher({
    ns: "gameManagement",
    work: "gameResult",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "returnBet",
      password: data.userPassword,
      formData: {
        uid: data.gameResultUid,
        reason: data.reasonOfModify
      }
    }
  })

  return result
}

// Game table manage

export const getGameTableList = async loginToken => {
  const result = await fetcher({
    ns: "Basic",
    work: "tableSpecies",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "tableSpecies"
      }
    }
  })

  return result
}

export const getGameTableMeta = async loginToken => {
  const result = await fetcher({
    ns: "Basic",
    work: "tableProperties",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "tableProperties"
      }
    }
  })

  return result
}

export const addGameTable = async (loginToken, data) => {
  const result = await fetcher({
    ns: "Basic",
    work: "tableSpecies",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        title: data.gameTableName,
        alias: data.gameTableNickname,
        code: data.gameTableCode,
        properties: data.gameTableProperties
      }
    }
  })

  return result
}

export const deleteGameTable = async (loginToken, uid) => {
  const result = await fetcher({
    ns: "Basic",
    work: "tableSpecies",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        uid: uid
      }
    }
  })

  return result
}

export const modifyGameTable = async (loginToken, data) => {
  const result = await fetcher({
    ns: "Basic",
    work: "tableSpecies",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: data
    }
  })

  return result
}

// Game type manage

export const getGameTypeSetList = async loginToken => {
  const result = await fetcher({
    ns: "Basic",
    work: "gameSets",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "gameSets"
      }
    }
  })

  return result
}

export const deletetGameType = async (loginToken, uid) => {
  const result = await fetcher({
    ns: "Basic",
    work: "gameSets",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        uid: uid
      }
    }
  })

  return result
}

export const modifyGameTypeOrder = async (loginToken, data) => {
  const result = await fetcher({
    ns: "Basic",
    work: "gameSets",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "orderSet",
      formData: data
    }
  })

  return result
}

// Game hall manage

export const addGameHall = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        title: data.gameHallName,
        themeTypeId: data.isRobotArmGameHall === true ? 1 : 0,
        sorting: data.index
      }
    }
  })

  return result
}

export const modifyGameHallOrder = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "orderSet",
      formData: data
    }
  })

  return result
}

export const modifyGameHall = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        title: data.gameHallName,
        uid: data.gameHallUid
      }
    }
  })

  return result
}

export const deleteGameHall = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      password: data.password,
      formData: {
        uidList: data.gameHallUidList
      }
    }
  })

  return result
}

export const enableGameHall = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "lock",
      password: data.password,
      formData: {
        uidList: data.gameHallUidList
      }
    }
  })

  return result
}

export const disableGameHall = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "unlock",
      password: data.password,
      formData: {
        uidList: data.gameHallUidList
      }
    }
  })

  return result
}
