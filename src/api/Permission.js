import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "SystemManage"

const replaceSpecialPermission = list => {
  const result = list.flatMap(x => {
    if (x === "basic") {
      return ["gameVendor", "casinoLocations", "serverSettings"]
    } else {
      return x
    }
  })

  return result
}

export const getPermissionRoleList = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list"
    }
  })

  return result
}

export const addPermissionRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        name: data.permissionRoleName,
        remark: data.comment,
        group: replaceSpecialPermission(data.permissions)
      }
    }
  })

  return result
}

export const getPermissionRoleDetail = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "detail",
      formData: {
        uid: data.permissionRoleUid
      }
    }
  })

  return result
}

export const updatePermissionRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        uid: data.permissionRoleUid,
        name: data.permissionRoleName,
        remark: data.comment,
        group: replaceSpecialPermission(data.permissions)
      }
    }
  })

  return result
}

export const deletePermissionRole = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      password: data.userPassword,
      formData: {
        uid: data.permissionRoleUid
      }
    }
  })

  return result
}

export const getAccountPermissionList = async loginToken => {
  const result = await fetcher({
    ns: "main",
    work: "Permission",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "reload"
    }
  })

  return result
}
