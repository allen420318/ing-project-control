import fetcher from "@/libs/fetcher.js"
import { getAPIFormatTimeString } from "@/libs/time.js"
import { filterBuilder, paginationBuilder, orderStatesBuilder } from "./helper.js"

const WORKING_DOMAIN = "reportManagement"

export const getOrdersByTimeRange = async (loginToken, data) => {
  const conditions = {
    conditions: {
      ...filterBuilder(data),
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        playerAccount: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "playerOrder",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "playerOrderByDateTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getOrdersByOrderNumber = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {},
      where: {
        betNo: data.gameOrderNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "playerOrder",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "playerOrderByBetNo",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getOrdersByTransactionNumber = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {},
      where: {
        serialNumber: data.orderTransactionNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "playerOrder",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "playerOrderBySerialNumber",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getOrdersByGameRound = async (loginToken, data) => {
  const conditions = {
    conditions: {
      ...filterBuilder(data),
      where: {
        shoeNo: data.gameShoesNumber,
        tableFrontId: data.gameDeskCode,
        roundNo: data.gameRoundNumber,
        playerAccount: data.playerAccount
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "playerOrder",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "playerOrderByRound",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Finance statistics

export const getFinanceStatisticOverview = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        agentGroup: data.topAgentAccount ? [data.topAgentAccount] : [],
        state: orderStatesBuilder(data.gameOrderStates)
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        playerAccount: data.playerAccount,
        currencyId: data.currencyId
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AccountingStatistics",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "accountingStatisticsByDateTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getFinanceStatisticPerPlayer = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        agentGroup: data.topAgentAccount ? [data.topAgentAccount] : [],
        state: orderStatesBuilder(data.gameOrderStates)
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        playerAccount: data.playerAccount,
        currencyId: data.currencyId
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "AccountingStatistics",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "accountingStatisticsByAgentGroup",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Transfer records

export const getTransferRecordsByTimeRange = async (loginToken, data) => {
  const conditions = {
    conditions: {
      filters: {
        transferType: data.transactionType ? [data.transactionType] : [],
        agentGroupUid: data.topAgentAccount ? data.topAgentAccount : ""
      },
      where: {
        timeRange: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        account: data.playerAccount,
        currencyId: data.currencyId
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "transferRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "transferRecordByDateTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getTransferRecordsByTransactionNumber = async (loginToken, data) => {
  const conditions = {
    conditions: {
      where: {
        transferId: data.orderTransactionNumber
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "transferRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "transferRecordByTransferId",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Credit changes

export const getCreditChangesOverview = async (loginToken, data) => {
  const conditions = {
    conditions: {
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        },
        playerAccount: data.playerAccount,
        agentGroupUid: data.topAgentAccount ? [data.topAgentAccount] : []
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "accountingVariance",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "AccountingVariance",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

// Abnormal result records

export const getAbnormalResultByTimeRange = async (loginToken, data) => {
  const topAgentAccount = data.topAgentAccount ? [data.topAgentAccount] : []

  const conditions = {
    conditions: {
      filters: {
        agentGroupUid: topAgentAccount
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        }
      },
      currencyId: data.currencyId
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "settleAbnormal",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "SettleAbnormalByDateTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getAbnormalResultByGameRound = async (loginToken, data) => {
  const topAgentAccount = data.topAgentAccount ? [data.topAgentAccount] : []
  const conditions = {
    conditions: {
      filters: { agentGroupUid: topAgentAccount },
      where: {
        tableId: data.gameTableSystemCode,
        shoeNo: data.gameShoesNumber,
        roundNo: data.gameRoundNumber,
        currencyId: data.currencyId
      }
    }
  }

  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "settleAbnormal",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "SettleAbnormalByRoundNo",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getAbnormalResultModifiedRecords = async (loginToken, data) => {
  const result = await fetcher({
    ns: "reportManagement",
    work: "settleAbnormal",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "betDetails",
      formData: {
        settleErrorLogUid: data.uid
      }
    }
  })
  return result
}

// Game records

export const getGameRecordsByTimeRange = async (loginToken, data) => {
  const gameHall = data.gameHall ? [data.gameHall] : []
  const gameType = data.gameType ? [data.gameType] : []
  const gameTable = data.gameTable ? [data.gameTable] : []

  const conditions = {
    conditions: {
      filters: {
        themes: gameHall,
        gameSets: gameType,
        tableSpecies: gameTable
      },
      where: {
        roundTime: {
          start: getAPIFormatTimeString(data.startDate),
          end: getAPIFormatTimeString(data.endDate)
        }
      }
    }
  }

  const result = await fetcher({
    ns: "gameManagement",
    work: "gameResult",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "gameResultByDateTime",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export const getGameRecordsByGameRound = async (loginToken, data) => {
  const gameHall = data.gameHall ? [data.gameHall] : []

  const conditions = {
    conditions: {
      filters: { themes: gameHall },
      where: {
        frontCode: data.gameDeskCode,
        shoeNo: data.gameShoesNumber,
        roundNo: data.gameRoundNumber
      }
    }
  }

  const result = await fetcher({
    ns: "gameManagement",
    work: "gameResult",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "gameResultByRound",
        ...paginationBuilder(data),
        ...conditions
      }
    }
  })
  return result
}

export default {
  getOrdersByTimeRange,
  getOrdersByOrderNumber,
  getOrdersByTransactionNumber,
  getOrdersByGameRound,
  getFinanceStatisticOverview,
  getFinanceStatisticPerPlayer,
  getTransferRecordsByTimeRange,
  getTransferRecordsByTransactionNumber,
  getCreditChangesOverview,
  getGameRecordsByTimeRange,
  getGameRecordsByGameRound,
  getAbnormalResultByTimeRange,
  getAbnormalResultModifiedRecords,
  getAbnormalResultByGameRound
}
