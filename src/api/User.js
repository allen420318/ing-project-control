import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "main"

export const checkLoginTokenStatus = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "noop",
    version: "0.0.1",
    loginToken: loginToken
  })
  return result
}

export const forceResetPassword = async data => {
  const result = await fetcher({
    ns: "main",
    work: "forceChangePassword",
    version: "0.0.1",
    loginToken: data.loginToken,
    data: {
      password: data.newPassword
    }
  })
  return result
}

export const resetPasswordByToken = async data => {
  const result = await fetcher({
    ns: "main",
    work: "changePasswordByToken",
    version: "0.0.1",
    data: {
      password: data.newPassword,
      token: data.resetPasswordToken
    }
  })
  return result
}

export const verifyResetPasswordToken = async resetPasswordToken => {
  const result = await fetcher({
    ns: "main",
    work: "forgetPasswordTokenVerify",
    version: "0.0.1",
    data: {
      token: resetPasswordToken
    }
  })
  return result
}

export const sendPasswordResetLink = async email => {
  const result = await fetcher({
    ns: "main",
    work: "forgetPassword",
    version: "0.0.1",
    data: {
      email: email
    }
  })
  return result
}

export const userLogin = async data => {
  const result = await fetcher({
    ns: "main",
    work: "login",
    version: "0.0.1",
    data: {
      id: data.id,
      password: data.pwd,
      tz: Math.abs(new Date().getTimezoneOffset())
    }
  })
  return result
}

export const userLogout = async loginToken => {
  const result = await fetcher({
    ns: "main",
    work: "logout",
    version: "0.0.1",
    loginToken: loginToken
  })
  return result
}

export const changePassword = async data => {
  const result = await fetcher({
    ns: "main",
    work: "changePassword",
    version: "0.0.1",
    loginToken: data.loginToken,
    data: {
      password: data.oldPassword,
      formData: {
        password: data.newPassword
      }
    }
  })
  return result
}

export const getUserList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "systemManage",
    work: "controller",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        account: data.userAccount
      }
    }
  })
  return result
}

export const addUser = async (loginToken, data) => {
  const result = await fetcher({
    ns: "systemManage",
    work: "controller",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        account: data.userAccount,
        nickname: data.nickname,
        password: data.password,
        email: data.email,
        permissionList: data.permissionRoles
      }
    }
  })
  return result
}

export const updateUser = async (loginToken, data) => {
  const result = await fetcher({
    ns: "systemManage",
    work: "controller",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        uid: data.userAccountUid,
        nickname: data.nickname,
        email: data.email,
        permissionList: data.permissionRoles
      }
    }
  })
  return result
}

export const deleteUser = async (loginToken, data) => {
  const result = await fetcher({
    ns: "systemManage",
    work: "controller",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      password: data.password,
      formData: {
        uid: data.userAccountUid
      }
    }
  })
  return result
}

export const disableUser = async (loginToken, data) => {
  const result = await fetcher({
    ns: "systemManage",
    work: "controller",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "lock",
      formData: {
        uid: data.userAccountUid
      }
    }
  })
  return result
}

export const enableUser = async (loginToken, data) => {
  const result = await fetcher({
    ns: "systemManage",
    work: "controller",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "unlock",
      formData: {
        uid: data.userAccountUid
      }
    }
  })
  return result
}

export default {
  checkLoginTokenStatus,
  forceResetPassword,
  resetPasswordByToken,
  verifyResetPasswordToken,
  sendPasswordResetLink,
  userLogin,
  userLogout,
  changePassword,
  getUserList,
  addUser,
  updateUser,
  deleteUser,
  disableUser,
  enableUser
}
