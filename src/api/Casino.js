import fetcher from "@/libs/fetcher.js"

const WORKING_DOMAIN = "gameManagement"

export const getCasinoLocations = async loginToken => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "basicItems",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      dataTypes: ["casinoLocations"]
    }
  })

  return result
}

export const getCasinoStreamCDNList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}

export const addCasinoStreamCDN = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        channelId: data.channelId,
        address: data.CDNAddress,
        remark: data.comment
      }
    }
  })

  return result
}

export const setCasinoStreamDefaultCDN = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "defaultOption",
      formData: {
        uid: data.CDNUid
      }
    }
  })

  return result
}

export const disableCasinoStreamCDN = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "lock",
      formData: {
        uid: data.CDNUid
      }
    }
  })

  return result
}

export const enableCasinoStreamCDN = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "unlock",
      formData: {
        uid: data.CDNUid
      }
    }
  })

  return result
}

export const deleteCasinoStreamCDN = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        uid: data.CDNUid
      }
    }
  })

  return result
}

export const editCasinoStreamCDN = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "cdnSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        cdnSettingUid: data.CDNUid,
        channelId: data.channelId,
        address: data.CDNAddress,
        remark: data.comment
      }
    }
  })

  return result
}

export const getCasinoStreamResolutionTypes = async loginToken => {
  const result = await fetcher({
    ns: "gameManagement",
    work: "basicItems",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      dataTypes: ["videoReceiver"]
    }
  })

  return result
}

export const getCasinoStreamResolutionList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoResolution",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        videoReceiverUid: data.streamResolutionTypeUid
      }
    }
  })

  return result
}

export const editCasinoStreamResolution = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoResolution",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        resolutionUid: data.streamResolutionUid,
        resolution: data.streamResolution
      }
    }
  })

  return result
}

export const getCasinoStreamGameTableList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "tableVideo",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "tableVideo",
        conditions: { where: { casinoLocationUid: data.casinoLocationUid } },
        pagination: { pageSize: data.itemsPerPage, pageIndex: data.pagingIndex }
      }
    }
  })

  return result
}

export const editCasinoStreamGameTable = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "tableVideo",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        tableSetUid: data.gameTableStreamUid,
        cdnSettingUid: data.CDNUid,
        appName: data.appName,
        streamName: data.streamName
      }
    }
  })

  return result
}

export const getCasinoStreamRecordList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}

export const addCasinoStreamRecord = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        address: data.url,
        remark: data.comment
      }
    }
  })

  return result
}

export const editCasinoStreamRecord = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        videoRecordUid: data.streamRecordUid,
        address: data.url,
        remark: data.comment
      }
    }
  })

  return result
}

export const setCasinoStreamDefaultRecord = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "defaultOption",
      formData: {
        uid: data.streamRecordUid
      }
    }
  })

  return result
}

export const deleteCasinoStreamRecord = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoRecord",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        uid: data.streamRecordUid
      }
    }
  })

  return result
}

export const getCasinoStreamPlayerData = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPlaySettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "detail",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}

export const editCasinoStreamPlayer = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPlaySettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        address: data.url,
        appName: data.appName,
        format: data.fileExtension
      }
    }
  })

  return result
}

export const getCasinoStreamWholeViewList = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "list",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}

export const addCasinoStreamWholeView = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "add",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        robotArmThemeUid: data.robotArmGameHallUid ? data.robotArmGameHallUid : "",
        title: data.wholeViewName,
        address: data.streamUrl,
        appName: data.appName,
        streamName: data.streamName
      }
    }
  })

  return result
}

export const editCasinoStreamWholeView = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        panoramaSettingUid: data.streamWholeViewUid,
        robotArmThemeUid: data.robotArmGameHallUid ? data.robotArmGameHallUid : "",
        title: data.wholeViewName,
        address: data.streamUrl,
        appName: data.appName,
        streamName: data.streamName
      }
    }
  })

  return result
}

export const enableCasinoStreamWholeView = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "unlock",
      formData: {
        uid: data.streamWholeViewUid
      }
    }
  })

  return result
}

export const disableCasinoStreamWholeView = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "lock",
      formData: {
        uid: data.streamWholeViewUid
      }
    }
  })

  return result
}

export const deleteCasinoStreamWholeView = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "delete",
      formData: {
        uid: data.streamWholeViewUid
      }
    }
  })

  return result
}

export const getCasinoStreamGameTableDelay = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "tableVideoDelay",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "detail",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}

export const setCasinoStreamGameTableDelay = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "tableVideoDelay",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "modify",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        tableVideoDelay: data.microSecondsOfDelay
      }
    }
  })

  return result
}

export const getCasinoGameHallList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "themes",
        pagination: { pageSize: data.itemsPerPage, pageIndex: data.pagingIndex },
        conditions: {
          filters: {
            casinoLocations: data.casinoLocationUid === "" ? [] : [data.casinoLocationUid]
          }
        }
      }
    }
  })

  return result
}

export const getCasinoRobotArmList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "themes",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "robotArmList",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}

export const getCasinoGameTableList = async (loginToken, data) => {
  const result = await fetcher({
    ns: WORKING_DOMAIN,
    work: "tableSets",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "report",
      formData: {
        name: "tableSets",
        pagination: { pageSize: data.itemsPerPage, pageIndex: data.pagingIndex },
        conditions: {
          filters: {
            themeUid: data.gameHallUid === "" ? [] : [data.gameHallUid]
          }
        }
      }
    }
  })

  return result
}

export const reloadCasinoGameTableStream = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "tableVideo",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "reload",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        tableSetUidList: data.gameTableUidList
      }
    }
  })

  return result
}

export const reloadCasinoWholeViewStream = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "videoPanoramaSettings",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "reload",
      formData: {
        casinoLocationUid: data.casinoLocationUid,
        themeUid: data.gameHallUid
      }
    }
  })

  return result
}

export const reloadCasinoGameTableStreamDelay = async (loginToken, data) => {
  const result = await fetcher({
    ns: "videoManagement",
    work: "tableVideoDelay",
    version: "0.0.1",
    loginToken: loginToken,
    data: {
      command: "reload",
      formData: {
        casinoLocationUid: data.casinoLocationUid
      }
    }
  })

  return result
}
