const menu = [
  {
    // 代理管理
    key: "代理管理",
    isActive: false,
    domId: {
      menu: "divAgentFuncSide",
      index: "labAgentFunc"
    },
    items: [
      {
        // 最高代理管理
        key: "最高代理管理",
        routeName: "TopAgentManage",
        isActive: false,
        domId: {
          menu: "btnTopAgentSide",
          index: "btnTopAgent"
        },
        apiName: "agent"
      }
    ]
  },
  {
    // 基本設定
    key: "基本設定",
    isActive: false,
    domId: {
      menu: "divBasicItemSide",
      index: "labBasicItem"
    },
    items: [
      {
        // 桌種設定
        key: "桌種設定",
        routeName: "TableTypeSetting",
        isActive: false,
        domId: {
          menu: "btnTableTypeSide",
          index: "btnTableType"
        },
        apiName: "tableSpecies"
      },
      {
        // 遊戲設定
        key: "遊戲設定",
        routeName: "GameTypeSetting",
        isActive: false,
        domId: {
          menu: "btnGameSetSide",
          index: "btnGameSet"
        },
        apiName: "gameSets"
      }
    ]
  },
  {
    // 遊戲管理
    key: "遊戲管理",
    isActive: false,
    domId: {
      menu: "divGameManageSide",
      index: "labGameManageFunc"
    },
    items: [
      {
        // 基本項目
        key: "基本項目",
        routeName: "BasicManage",
        isActive: false,
        domId: {
          menu: "btnBasicItemSide",
          index: "btnBasicItem"
        },
        apiName: "basic"
      },
      {
        // 廳館管理
        key: "廳館管理",
        routeName: "GameHallManage",
        isActive: false,
        domId: {
          menu: "btnRoomManageSide",
          index: "btnRoomManage"
        },
        apiName: "themes"
      },
      {
        // 賭桌管理
        key: "賭桌管理",
        routeName: "BetTableManage",
        isActive: false,
        domId: {
          menu: "btnTableManageSide",
          index: "btnTableManage"
        },
        apiName: "tableSets"
      },
      {
        // 遊戲紀錄
        key: "遊戲紀錄",
        routeName: "GameRecords",
        isActive: false,
        domId: {
          menu: "btnGameRecordSide",
          index: "btnGameRecord"
        },
        apiName: "gameResult"
      },
      {
        // 遊戲限紅
        key: "遊戲限紅",
        routeName: "GameLimit",
        isActive: false,
        domId: {
          menu: "btnBetLimitSide",
          index: "btnBetLimit"
        },
        apiName: "redLimits"
      }
    ]
  },
  {
    // 報表功能
    key: "報表功能",
    isActive: false,
    domId: {
      menu: "divReportFuncSide",
      index: "labReportFunc"
    },
    items: [
      {
        // 轉帳紀錄
        key: "轉帳紀錄",
        routeName: "TransferRecords",
        isActive: false,
        domId: {
          menu: "btnTransferRecordSide",
          index: "btnTransferRecord"
        },
        apiName: "transferRecord"
      },
      {
        // 帳務統計
        key: "帳務統計",
        routeName: "FinanceStatistics",
        isActive: false,
        domId: {
          menu: "btnTotalAccountingSide",
          index: "btnTotalAccounting"
        },
        apiName: "accountingStatistics"
      },
      {
        // 會員訂單
        key: "會員訂單",
        routeName: "MemberBetRecords",
        isActive: false,
        domId: {
          menu: "btnBetOrderSide",
          index: "btnBetOrder"
        },
        apiName: "playerOrder"
      },
      {
        // 結算異常查詢
        key: "結算異常查詢",
        routeName: "AbnormalResultRecords",
        isActive: false,
        domId: {
          menu: "btnBillAbnormalSearchSide",
          index: "btnBillAbnormalSearch"
        },
        apiName: "abnormal"
      },
      {
        // 帳變報表
        key: "帳變報表",
        routeName: "CreditChanges",
        isActive: false,
        domId: {
          menu: "btnAccountChangeSide",
          index: "btnAccountChange"
        },
        apiName: "accountingVariance"
      }
    ]
  },
  {
    // 系統管理
    key: "系統管理",
    isActive: false,
    domId: {
      menu: "divSystemFuncSide",
      index: "labSystemFunc"
    },
    items: [
      {
        // 角色權限管理
        key: "角色權限管理",
        routeName: "PermissionManage",
        isActive: false,
        domId: {
          menu: "btnCharPermissionSide",
          index: "btnCharPermission"
        },
        apiName: "permission"
      },
      {
        // 後台使用者管理
        key: "後台使用者管理",
        routeName: "UserManage",
        isActive: false,
        domId: {
          menu: "btnBackUserSide",
          index: "btnBackUser"
        },
        apiName: "controller"
      }
    ]
  },
  {
    // 維護管理
    key: "維護管理",
    isActive: false,
    domId: {
      menu: "divMaintainManaSide",
      index: "labMaintain"
    },
    items: [
      {
        // 系統維護
        key: "系統維護",
        routeName: "MaintainManage",
        isActive: false,
        domId: {
          menu: "btnSystemMaintainSide",
          index: "btnSystemMaintain"
        },
        apiName: "maintain"
      },
      {
        // 代理維護
        key: "代理維護",
        routeName: "AgentMaintainManage",
        isActive: false,
        domId: {
          menu: "btnAgentMainSide",
          index: "btnAgentMaintain"
        },
        apiName: "maintainAgent"
      }
    ]
  }
]

export default {
  menu: menu
}
