const index = () => import("../../views/index.vue")
const PageNotFound = () => import("../../views/PageNotFound.vue")
const Login = () => import("../../views/Login.vue")
const Home = () => import("../../views/Home.vue")
const ForgetPassword = () => import("../../views/ForgetPassword.vue")
const ResetPassword = () => import("../../views/ResetPassword.vue")

//桌種設定
const TableTypeSetting = () => import("../../views/TableTypeSetting.vue")
//遊戲設定
const GameTypeSetting = () => import("../../views/GameTypeSetting.vue")
//基本項目
const BasicManage = () => import("../../views/BasicManage.vue")
//賭桌管理
const BetTableManage = () => import("../../views/BetTableManage.vue")
//遊戲限紅
const GameLimit = () => import("../../views/GameLimitV2.vue")
//轉帳紀錄
const TransferRecords = () => import("../../views/TransferRecords.vue")
//帳務統計
const FinanceStatistics = () => import("../../views/FinanceStatistics.vue")
//結算異常查詢
const AbnormalResultRecords = () => import("../../views/AbnormalResultRecords.vue")
//角色權限管理
const PermissionManage = () => import("../../views/PermissionManage.vue")
//後台使用者管理
const UserManage = () => import("../../views/UserManage.vue")
//代理維護
const AgentMaintainManage = () => import("../../views/AgentMaintainManage.vue")
//最高代理管理
const TopAgentManage = () => import("../../views/TopAgentManage.vue")
//帳變報表
const CreditChanges = () => import("../../views/CreditChanges.vue")

/**
 *  Page content is different on ELE & REAL environments
 */

//廳館管理
const GameHallManage = () => import("../../views/projects/real/GameHallManage.vue")
//視訊管理
const StreamManage = () => import("../../views/projects/real/StreamManage.vue")
//視訊設定
const StreamSetting = () => import("../../views/projects/real/StreamSetting.vue")
//會員訂單
const MemberBetRecords = () => import("../../views/projects/real/MemberBetRecords.vue")
//遊戲紀錄
const GameRecords = () => import("../../views/projects/real/GameRecords.vue")
//系統維護
const MaintainManage = () => import("../../views/projects/real/MaintainManage.vue")

const main = [
  {
    name: "index",
    path: "index.html",
    isEnabled: true,
    component: index,
    alias: ["/"]
  },
  {
    name: "TopAgentManage",
    path: "TopAgentManage.html",
    isEnabled: true,
    component: TopAgentManage
  },
  {
    name: "TableTypeSetting",
    path: "TableTypeSetting.html",
    isEnabled: false,
    component: TableTypeSetting
  },
  {
    name: "GameTypeSetting",
    path: "GameTypeSetting.html",
    isEnabled: false,
    component: GameTypeSetting
  },
  {
    name: "BasicManage",
    path: "BasicManage.html",
    isEnabled: true,
    component: BasicManage
  },
  {
    name: "GameHallManage",
    path: "GameHallManage.html",
    isEnabled: true,
    component: GameHallManage
  },
  {
    name: "BetTableManage",
    path: "BetTableManage.html",
    isEnabled: true,
    component: BetTableManage
  },
  {
    name: "GameRecords",
    path: "GameRecords.html",
    isEnabled: true,
    component: GameRecords
  },
  {
    name: "GameLimit",
    path: "GameLimit.html",
    isEnabled: true,
    component: GameLimit
  },
  {
    name: "TransferRecords",
    path: "TransferRecords.html",
    isEnabled: true,
    component: TransferRecords
  },
  {
    name: "FinanceStatistics",
    path: "FinanceStatistics.html",
    isEnabled: true,
    component: FinanceStatistics
  },
  {
    name: "MemberBetRecords",
    path: "MemberBetRecords.html",
    isEnabled: true,
    component: MemberBetRecords
  },
  {
    name: "AbnormalResultRecords",
    path: "AbnormalResultRecords.html",
    isEnabled: true,
    component: AbnormalResultRecords
  },
  {
    name: "StreamManage",
    path: "StreamManage.html",
    isEnabled: false,
    component: StreamManage
  },
  {
    name: "StreamSetting",
    path: "StreamSetting.html",
    isEnabled: false,
    component: StreamSetting
  },
  {
    name: "PermissionManage",
    path: "PermissionManage.html",
    isEnabled: false,
    component: PermissionManage
  },
  {
    name: "UserManage",
    path: "UserManage.html",
    isEnabled: false,
    component: UserManage
  },
  {
    name: "MaintainManage",
    path: "MaintainManage.html",
    isEnabled: false,
    component: MaintainManage
  },
  {
    name: "AgentMaintainManage",
    path: "AgentMaintainManage.html",
    isEnabled: false,
    component: AgentMaintainManage
  },
  {
    name: "CreditChanges",
    path: "CreditChanges.html",
    isEnabled: false,
    component: CreditChanges
  }
]

const router = [
  {
    name: "Login",
    path: "/login.html",
    isEnabled: true,
    component: Login
  },
  {
    path: "/main",
    isEnabled: true,
    component: Home,
    children: main,
    alias: [""]
  },
  {
    name: "ForgetPassword",
    path: "/forgetPwd.html",
    isEnabled: true,
    component: ForgetPassword
  },
  {
    name: "ResetPassword",
    path: "/resetPassword.html",
    isEnabled: true,
    component: ResetPassword
  },
  {
    name: "PageNotFound",
    path: "*",
    isEnabled: true,
    component: PageNotFound
  }
]

export default {
  router
}
