import { SET_MENU_PERMISSION_LIST, SET_MENU_LIST } from "../mutation-types.js"

import { getAccountPermissionList } from "@/api/Permission.js"

const state = {
  menu: [],
  menuPermissionList: []
}

const actions = {
  async setMenuPermissionList({ commit }, loginToken) {
    const { exitCode, data } = await getAccountPermissionList(loginToken)

    if (exitCode === true && data.length > 0) {
      commit(SET_MENU_PERMISSION_LIST, data)
    } else {
      throw new Error("Account permission list request failed")
    }
  },
  setMenuList({ commit }, menu) {
    commit(SET_MENU_LIST, menu)
  }
}

const mutations = {
  [SET_MENU_PERMISSION_LIST](state, data) {
    state.menuPermissionList = data
  },
  [SET_MENU_LIST](state, data) {
    state.menu = data
  }
}

const getters = {
  Rows: state => {
    return state.menu
  },
  getMenuList: state => {
    return state.menu.map(rootMenu => {
      const subMenu = rootMenu.items.map(sub => {
        return { apiName: sub.apiName, key: sub.key }
      })

      return { key: rootMenu.key, subMenu: subMenu }
    })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
