const _ModuleName = "agent"

const store = {
  namespaced: true,
  state: {
    agentInfo: [],
    ModuleName: _ModuleName
  },
  mutations: {
    setObject(state, data) {
      state.agentInfo = data
    }
  },
  actions: {
    setAgentInfo({ commit }, data) {
      commit("setObject", data)
    }
  },
  getters: {
    agentInfo: state => {
      var re = state.agentInfo
      return re
    },
    storeKey: state => {
      return state.storeKey
    }
  }
}

export default store
