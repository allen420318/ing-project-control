const store = {
  namespaced: true,
  state: {
    betRecordSearch: {
      //betState : '',
      //playerAccount : '',
      //start : '',
      //end : ''
    }
  },
  mutations: {
    setObject(state, Payload) {
      state[Payload.item] = Payload.data
    }
  },
  actions: {
    Set_betRecordSearch({ commit }, data) {
      commit("setObject", { item: "betRecordSearch", data: data })
    },
    clear_betRecordSearch({ commit }) {
      commit("setObject", { item: "betRecordSearch", data: {} })
    }
  },
  getters: {
    Get_betRecordSearch: state => {
      return state.betRecordSearch
    },

    Rows: state => {
      return state.menuItems
    }
  }
}

export default store
