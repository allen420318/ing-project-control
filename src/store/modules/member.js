import Storage from "../../libs/storage.js"
import { REMOVE_USER_LOGIN_DATA, SET_USER_LOGIN_DATA } from "../mutation-types.js"

const storeKey = "__member__"
const initValue = Storage.getItem(storeKey)

const state = {
  id: initValue.id || "",
  nickname: initValue.nickname || "",
  loginToken: initValue.loginToken || "",
  forceChangePassword: initValue.forceChangePassword || false,
  storeKey: storeKey
}

const actions = {
  removeUserLoginData({ commit }) {
    commit(REMOVE_USER_LOGIN_DATA)
  },
  setUserLoginData({ commit }, data) {
    commit(SET_USER_LOGIN_DATA, data)
  }
}

const mutations = {
  [REMOVE_USER_LOGIN_DATA](state) {
    state.id = ""
    state.nickname = ""
    state.loginToken = ""
    state.forceChangePassword = ""
    Storage.clear(storeKey)
  },
  [SET_USER_LOGIN_DATA](state, { id, nickname, loginToken, forceChangePassword }) {
    state.id = id
    state.nickname = nickname
    state.loginToken = loginToken
    state.forceChangePassword = forceChangePassword
    Storage.setItem(storeKey, { id, nickname, loginToken, forceChangePassword })
  }
}

const getters = {
  isLogin: state => {
    return !(state.loginToken === "")
  },
  loginToken: state => {
    return state.loginToken
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
