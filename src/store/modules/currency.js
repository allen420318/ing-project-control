import { FETCH_CURRENCY_LIST } from "../mutation-types.js"
import { getCurrencyList } from "@/api/Currency.js"

const state = {
  currencies: []
}

const actions = {
  async fetchCurrencyList({ commit, rootState }) {
    const res = await getCurrencyList(rootState.member.loginToken)

    if (res.exitCode === true) {
      commit(FETCH_CURRENCY_LIST, res.data)
    }
  }
}

const mutations = {
  [FETCH_CURRENCY_LIST](state, payload) {
    state.currencies = payload
  }
}

const getters = {
  getCurrencyIdList: state => {
    return state.currencies.map(x => x.currencyId)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
