const _ModuleName = "game"

const store = {
  namespaced: true,
  state: {
    gameVendor: [],
    tableTypes: [],
    casinoLocations: [],
    serverTypes: [],
    serverSettings: [],
    halls: [],
    betTables: [],
    gameTypes: [],
    tableLimits: [],
    videoReceiver: [],
    ModuleName: _ModuleName
  },
  mutations: {
    setObject(state, Payload) {
      state[Payload.item] = Payload.data
    }
  },
  actions: {
    Set_gameVendor({ commit }, data) {
      commit("setObject", { item: "gameVendor", data: data })
    },
    Set_tableTypes({ commit }, data) {
      commit("setObject", { item: "tableTypes", data: data })
    },
    Set_serverTypes({ commit }, data) {
      commit("setObject", { item: "serverTypes", data: data })
    },
    Set_casinoLocations({ commit }, data) {
      commit("setObject", { item: "casinoLocations", data: data })
    },
    Set_serverSettings({ commit }, data) {
      commit("setObject", { item: "serverSettings", data: data })
    },
    Set_halls({ commit }, data) {
      commit("setObject", { item: "halls", data: data })
    },
    Set_betTables({ commit }, data) {
      commit("setObject", { item: "betTables", data: data })
    },
    Set_gameTypes({ commit }, data) {
      commit("setObject", { item: "gameTypes", data: data })
    },
    Set_tableLimits({ commit }, data) {
      commit("setObject", { item: "tableLimits", data: data })
    },
    Set_videoReceiver({ commit }, data) {
      commit("setObject", { item: "videoReceiver", data: data })
    }
  },
  getters: {
    Get_gameVendor: state => {
      return state.gameVendor
    },
    Get_tableTypes: state => {
      return state.tableTypes
    },
    Get_serverTypes: state => {
      return state.serverTypes
    },
    Get_casinoLocations: state => {
      return state.casinoLocations
    },
    Get_serverTypesUid: state => serverTypeAlias => {
      const [result] = state.serverTypes.filter(x => x.alias === serverTypeAlias)

      return result
    },
    Get_serverSetting: (state, getters) => serverTypeAlias => {
      const serverTypeUid = getters.Get_serverTypesUid(serverTypeAlias)
      const [result] = state.serverSettings.filter(x => x.serverTypesUid === serverTypeUid)

      return result
    },
    Get_halls: state => {
      return state.halls
    },
    Get_betTables: state => {
      return state.betTables
    },
    Get_gameTypes: state => {
      return state.gameTypes
    },
    Get_tableLimits: state => {
      return state.tableLimits
    },
    Get_videoReceiver: state => {
      return state.videoReceiver
    },
    storeKey: state => {
      return state.storeKey
    }
  }
}

export default store
