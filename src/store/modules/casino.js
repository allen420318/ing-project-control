import { FETCH_CASINO_LOCATIONS } from "../mutation-types.js"
import { getCasinoLocations } from "@/api/Casino.js"

const state = {
  casinoLocations: []
}

const actions = {
  async fetchCasinoLocations({ commit, rootState }) {
    const { exitCode, data } = await getCasinoLocations(rootState.member.loginToken)

    if (exitCode === true) {
      const { casinoLocations } = data
      const payload = casinoLocations ? [...casinoLocations] : []
      commit(FETCH_CASINO_LOCATIONS, payload)
    } else {
      throw { res: { data } }
    }
  }
}

const mutations = {
  [FETCH_CASINO_LOCATIONS](state, payload) {
    state.casinoLocations = payload
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
