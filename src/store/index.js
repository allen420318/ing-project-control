import Vue from "vue"
import Vuex from "vuex"

import project from "./modules/project.js"
import member from "./modules/member.js"
import menu from "./modules/menu.js"
import agent from "./modules/agent.js"
import game from "./modules/game.js"
import tempData from "./modules/tempdata.js"
import error from "./modules/error.js"
import cache from "./modules/cache.js"
import currency from "./modules/currency.js"
import casino from "./modules/casino.js"
import i18n from "./modules/i18n.js"

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    project,
    member,
    menu,
    agent,
    game,
    tempData,
    error,
    cache,
    currency,
    casino,
    i18n
  },
  strict: process.env.NODE_ENV !== "production"
})

export default store
