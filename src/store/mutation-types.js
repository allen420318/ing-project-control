// Language
export const LOAD_LANGUAGE_FILE = "⛩🐳 Load localization data from json file."
export const SET_DEFAULT_LANGUAGE_CODE = "⛩🐳 Set default language code."

// Project
export const SET_PROJECT_TITLE = "🐵 Set project title object from global space."
export const SET_PROJECT_API_SETTINGS = "🐵 Set project API config from global space."

// Menu
export const SET_MENU_PERMISSION_LIST = "🐧 Fetch and set menu permission list."
export const SET_MENU_LIST = "🐧 Set menu root and sub menu data list"

// Member
export const REMOVE_USER_LOGIN_DATA = "🖐💎⚔ Remove login token and data on storage ⚔"
export const SET_USER_LOGIN_DATA = "💎 Save user login data to local storage and vuex"

// Error
export const SET_FORCE_RESET_USER_PASSWORD_ERROR = "👻 Enconter error when force reset user password"
export const REMOVE_FORCE_RESET_USER_PASSWORD_ERROR = "🖐👻 Remove error state about force reset user password"
export const SET_RESET_PASSWORD_TOKEN_ERROR = "👻 Enconter error with reset user password token verify"
export const REMOVE_RESET_PASSWORD_TOKEN_ERROR = "🖐👻 Remove error state about reset user password token verify"

// Cache
export const SET_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🏄 Set search conditions for member bet records fetch list"
export const REMOVE_MEMBER_BET_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🖐🏄 Remove search conditions for member bet records fetch list"
export const SET_TOP_AGENT_LIST_SEARCH_CONDITIONS_CACHE = "🏄 Set search conditions for top agent fetch list"
export const REMOVE_TOP_AGENT_LIST_SEARCH_CONDITIONS_CACHE = "🖐🏄 Remove search conditions for top agent fetch list"
export const SET_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE =
  "🏄 Set search conditions for finance statistics fetch list"
export const REMOVE_FINANCE_STATISTICS_LIST_SEARCH_CONDITIONS_CACHE =
  "🖐🏄 Remove search conditions for finance statistics fetch list"
export const SET_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE = "🏄 Set search conditions for credit changes fetch list"
export const REMOVE_CREDIT_CHANGES_LIST_SEARCH_CONDITIONS_CACHE =
  "🖐🏄 Remove search conditions for credit changes fetch list"
export const SET_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🏄 Set search conditions for transfer records fetch list"
export const REMOVE_TRANSFER_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🖐🏄 Remove search conditions for transfer records fetch list"
export const SET_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE = "🏄 Set search conditions for game records fetch list"
export const REMOVE_GAME_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🖐🏄 Remove search conditions for game records fetch list"
export const SET_ABNORMAL_RESULT_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🏄 Set search conditions for abnormal result records fetch list"
export const REMOVE_ABNORMAL_RESULT_RECORDS_LIST_SEARCH_CONDITIONS_CACHE =
  "🖐🏄 Remove search conditions for abnormal result records fetch list"

// Currency
export const FETCH_CURRENCY_LIST = "🎃 Fetch currency list from api"

// Casino
export const FETCH_CASINO_LOCATIONS = "🎰 Fetch casino location data"
