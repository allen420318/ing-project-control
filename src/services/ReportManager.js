import baseService from "./base.js"

const ns = "reportManagement"

class ReportManager extends baseService {
  constructor() {
    super()
    //this.selfParam = 'X';
    this.RequestBItem = {
      tableTypes: "tableTypes",
      themes: "themes",
      gameTypes: "gameTypes",
      gameSets: "gameSets"
    }
  }

  //報表-轉帳紀錄  (廢棄)
  getTransferRecords(loginToken, d) {
    var where = {
      account: d.account,
      billNumber: d.billNumber
    }

    if (d.start) {
      where.roundTime = {
        start: d.start,
        end: d.end
      }
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "transferRecords",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "agentPlatformTransferRecords",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                transferTypes: d.transferTypes
              },
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //報表-遊戲紀錄 By Time
  getGmRecordsByTime(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "gameRecords",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "gameRecordsByTime",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes,
                tableTypes: d.tableTypes,
                gameTypes: d.gameTypes
              },
              where: {
                roundTime: {
                  start: d.start,
                  end: d.end
                }
              }
            }
          }
        }
      },
      {}
    )
  }

  //報表-遊戲紀錄 By RoundNo
  getGmRecordsByRoundNo(loginToken, d) {
    var where = {
      tableId: d.tableId,
      shoeNo: d.shoeNo,
      roundNo: d.roundNo
    }

    if (d.start) {
      where.roundTime = {
        start: d.start,
        end: d.end
      }
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "gameRecords",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "gameRecordsByRoundNo",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes
              },
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //取得交易類型列表
  getTransferType(loginToken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "getTransferType",
        version: "0.0.1",
        loginToken: loginToken
      },
      {}
    )
  }

  //取得遊戲(注單)狀態列表
  getGameResultState(loginToken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "getGameResultState",
        version: "0.0.1",
        loginToken: loginToken
      },
      {}
    )
  }

  //取得會員注單列表 (廢棄)
  getMemberBet(loginToken, d) {
    var where = {
      tableId: d.tableId,
      shoeNo: d.shoeNo,
      roundNo: d.roundNo,
      account: d.account,
      betNo: d.betNo
    }

    if (d.start) {
      where.roundTime = {
        start: d.start,
        end: d.end
      }
    }

    var filters = {
      themes: d.themes,
      states: d.states
    }

    var conditions = {
      where: where,
      filters: filters
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "memberBetNo",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "agentPlatformMemberBetNo",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes,
                states: d.states
              },
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //取得基本項目
  getBasicItems(logintoken, dataTypeList) {
    var dataTypeList = dataTypeList || this.basicItem
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "basicItems",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          dataTypes: dataTypeList
        }
      },
      {}
    )
  }

  //取得登入最高代理底下報表權限
  getReportPermission(logintoken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "reportPermission",
        version: "0.0.1",
        loginToken: logintoken
      },
      {}
    )
  }

  //報表-帳務統計(顯示最高代理統計)
  getAgentPlatformFinanceStatistics(loginToken, d) {
    var where = {}

    if (d.start) {
      where.roundTime = {
        start: d.start,
        end: d.end
      }
    }

    if (d.account) {
      where.playerAccount = d.account
    }

    if (d.states.length <= 0) {
      d.states = [1, 2, 3, 4]
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "AccountingStatistics",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "accountingStatisticsByDateTime",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                agentGroup: d.groupUid,
                state: d.states
              },
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //報表-帳務統計(顯示玩家帳號帳務統計)
  getAgentPlatformPlayerFinanceStatistics(loginToken, d) {
    var where = {}

    if (d.start) {
      where.roundTime = {
        start: d.start,
        end: d.end
      }
    }

    if (d.account) {
      where.playerAccount = d.account
    }

    if (d.states.length <= 0) {
      d.states = [1, 2, 3, 4]
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "AccountingStatistics",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "accountingStatisticsByAgentGroup",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                state: d.states,
                agentGroup: d.groupUid
              },
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //報表-遊戲紀錄 By Time version 2
  getGmRByTime(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "gameResults",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "IBOGameResult",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes,
                gameSets: d.gameTypes,
                tableSpecies: d.tableTypes
              },
              where: {
                roundTime: {
                  start: d.start,
                  end: d.end
                }
              }
            }
          }
        }
      },
      {}
    )
  }
  //報表-遊戲紀錄 By RoundNo version 2
  getGmRByRoundNo(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "gameResults",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "IBOGameResultByRoundNo",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes
              },
              where: {
                tableFrontId: d.tableFrontId,
                shoeNo: d.shoeNo,
                roundNo: d.roundNo
              }
            }
          }
        }
      },
      {}
    )
  }
  //報表-遊戲紀錄 version 2
  getGameResult(loginToken, d, item) {
    var re

    switch (item) {
      case "ByTime":
        re = this.getGmRByTime(loginToken, d)
        break

      case "ByRoundNo":
        re = this.getGmRByRoundNo(loginToken, d)
        break
    }

    return re
  }

  //報表-轉帳紀錄  By Time
  getTransferRecordsByTime(loginToken, d) {
    var where = { account: d.account }

    where.createDate = {
      start: d.start,
      end: d.end
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "transferRecord",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "transferRecordByDateTime",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                transferType: d.transferTypes,
                agentGroupUid: d.agentGroup
              },
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //報表-轉帳紀錄  By billNumber
  getTransferRecordsBybillNumber(loginToken, d) {
    var where = {
      transferId: d.billNumber
    }

    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "transferRecord",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "transferRecordByTransferId",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              where: where
            }
          }
        }
      },
      {}
    )
  }

  //報表-轉帳紀錄 version 2
  getTransferRecordsV2(loginToken, d, item) {
    var re

    switch (item) {
      case "ByTime":
        re = this.getTransferRecordsByTime(loginToken, d)
        break

      case "ByNumber":
        re = this.getTransferRecordsBybillNumber(loginToken, d)
        break
    }

    return re
  }

  //取得會員注單列表 ByTime
  getMemberBetByTime(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "playerOrder",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "playerOrderByDateTime",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes,
                state: d.states,
                gameSets: d.gameSets,
                agentGroup: d.agentGroup
              },
              where: {
                roundTime: {
                  start: d.start,
                  end: d.end
                },
                playerAccount: d.account
              }
            }
          }
        }
      },
      {}
    )
  }

  //取得會員注單列表 IBOGameBetLogsBybetNo
  getMemberBetBybetNo(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "playerOrder",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "playerOrderByBetNo",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              where: {
                betNo: d.betNo
              }
            }
          }
        }
      },
      {}
    )
  }

  //取得會員注單列表 IBOGameBetLogsByroundNo
  getMemberBetByroundNo(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "playerOrder",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "playerOrderByRound",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                themes: d.themes,
                agentGroup: d.agentGroup
              },
              where: {
                shoeNo: d.shoeNo,
                frontCode: d.tableId,
                roundNo: d.roundNo,
                playerAccount: d.account
              }
            }
          }
        }
      },
      {}
    )
  }

  //取得會員注單列表 BybillNumber
  getMemberBetBybillNumber(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "playerOrder",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "playerOrderBySerialNumber",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              where: {
                serialNumber: d.billNumber
              }
            }
          }
        }
      },
      {}
    )
  }

  //取得會員注單交易序號詳情
  getMemberBetTransactionNumber(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "playerOrder",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "serialDetails",
          formData: {
            betNo: d.betNo,
            gameRoundStartTime: d.gameRoundStartTime
          }
        }
      },
      {}
    )
  }

  //報表-投注紀錄 version 2
  getMemberBetV2(loginToken, d, item) {
    var re

    switch (item) {
      case "ByTime":
        re = this.getMemberBetByTime(loginToken, d)
        break

      case "ByBetNo":
        re = this.getMemberBetBybetNo(loginToken, d)
        break

      case "ByRoundNo":
        re = this.getMemberBetByroundNo(loginToken, d)
        break

      case "ByNumber":
        re = this.getMemberBetBybillNumber(loginToken, d)
        break
    }

    return re
  }

  //取得結算異常列表(時間)
  getSettlementExceptionByTime(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "settleAbnormal",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "SettleAbnormalByDateTime",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                agentGroupUid: d.agentGroup,
              },
              where: {
                roundTime: {
                  start: d.start,
                  end: d.end
                },
                currencyId: d.currencyId
              }
            }
          }
        }
      },
      {}
    )
  }

  //取得結算異常列表(遊戲局號)
  getSettlementExceptionByRoundNo(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "settleAbnormal",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "report",
          formData: {
            name: "SettleAbnormalByRoundNo",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                agentGroupUid: d.agentGroup,
              },
              where: {
                tableId: d.tableId,
                shoeNo: d.shoeNo,
                roundNo: d.roundNo,
                currencyId: d.currencyId
              },
            }
          }
        }
      },
      {}
    )
  }

  //取得結算異常下注詳情
  getSettlementExceptionBetSettlement(loginToken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "settleAbnormal",
        version: "0.0.1",
        loginToken: loginToken,
        data: {
          command: "betDetails",
          formData: {
            settleErrorLogUid: d
          }
        }
      },
      {}
    )
  }

  //報表-結算異常
  getSettlementException(loginToken, d, item) {
    var re

    switch (item) {
      case "ByTime":
        re = this.getSettlementExceptionByTime(loginToken, d)
        break

      case "ByRoundNo":
        re = this.getSettlementExceptionByRoundNo(loginToken, d)
        break
    }

    return re
  }

  getTopAgentWallet(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: "agentFeatures",
        work: "topAgent",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "walletOptions",
          formData: {
            wallet: d
          }
        }
      },
      {}
    )
  }
}

export default new ReportManager()
