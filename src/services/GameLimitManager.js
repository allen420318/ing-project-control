import baseService from './base.js';


let ns = 'Basic';
let testApiHost = "http://"+location.host+":3000/";

class GameLimitManager extends baseService{
  
   constructor() {
        super();
    }
  
  //取得遊戲全域設定
  getGameGlobalSets(logintoken){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getGameTypeList";
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'globalSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail'
		}		
	},{});
  }
  
  
   //修改遊戲全域設定 - 入桌限制
  updateEnterTableLimit(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'globalSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData :{
				type : 'enterTableLimit',
				'value' : d.v				
			}
		}		
	},{});
  }
  
   //修改遊戲全域設定 -  代理可啟用的限紅組數
  updateMaxLimitGroupNum(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'globalSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData :{
				type : 'redLimitsMaxEnabledCount',
				'value' : d.v				
			}
		}		
	},{});
  }
  
   //修改遊戲全域設定 - 預設限紅
  updateDefaultLimit(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'globalSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData :{
				type : 'limit',
				uid : d.uid,
				lowerLimit : d.lowerLimit,
				upperLimit : d.upperLimit
			}
		}		
	},{});
  }
  
  
  //取得代理限紅
  getAgentLimits(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getGameTypeList";
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'redLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : "redLimits",
				conditions : {
					where : {
						agentGroupUid : d.agentGroupUid
					}
				}
			}
			
		}		
	},{});
  }
  
  
   //取得試算結果
  getTrialLimit(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getGameTypeList";
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'redLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'trial',
			formData : {				
				gameSetUid: d.gameSetUid,
				lowerLimit: d.lowerLimit,
				upperLimit: d.upperLimit
			}
			
		}		
	},{});
  }
  
  
  //新增代理限紅
  addTableLimit(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'redLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData :{
				agentGroupUid : d.agentGroupUid,
				code : d.code,
				lowerLimit : d.lowerLimit,
				upperLimit : d.upperLimit
			}
		}		
	},{});
  }	
  
  
  //修改代理限紅
  updateTableLimit(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'redLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData :{				
				uid : d.uid,
				lowerLimit : d.lowerLimit,
				upperLimit : d.upperLimit
			}
		}		
	},{});
  }	
  
  //刪除代理限紅
  delTableLimit(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'redLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			formData :{				
				agentGroupUid : d.agentGroupUid,
				redLimitUid : d.redLimitUid
			}
		}		
	},{});
  }	
  
  
  
   //修改代理賭桌最高限紅
  updateTableMaxLimit(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : 'GameManagement',
		work : 'redLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'set',
			formData :{				
				agentGroupUid : d.agentGroupUid,
				maxTableLimit : d.maxTableLimit
				
			}
		}		
	},{});
  }	
  
  
  
}

export default new GameLimitManager()