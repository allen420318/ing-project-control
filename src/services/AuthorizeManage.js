import baseService from './base.js';

let ns = 'SystemManage';
class AuthorizeManage extends baseService{
  
   constructor() {
        super();        
    }

	//角色權限列表
    getRolePermissions(logintoken){
		return this.$http.post(this.url,{
			ns : ns,
			work : 'Permission',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				command : 'list'			
			}		
		},{});
	}
	//新增角色權限
	addRolePermissions(logintoken,d){
		return this.$http.post(this.url,{
			ns : ns,
			work : 'Permission',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				command : 'add',
				formData : {
					name : d.name,
					group : d.permission,
					remark : d.remark
				}
			}		
		},{});
  	}
	//編輯角色權限
	editRolePermissions(logintoken,d){
		return this.$http.post(this.url,{
			ns : ns,
			work : 'Permission',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				command : 'modify',
				formData : {
					uid: d.uid,
					name : d.name,
					group : d.group,
					remark : d.remark
				}
			}		
		},{});
	  }
	  //角色權限詳細資料
	RolePermissionsDetail(logintoken,d){
		return this.$http.post(this.url,{
			ns : ns,
			work : 'Permission',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				command : 'detail',
				formData : {
					uid : d
				}
			}		
		},{});
	  }
	  //刪除角色權限
	DelRolePermissions(logintoken,d){
		return this.$http.post(this.url,{
			ns : ns,
			work : 'Permission',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				command : 'delete',
				password : d.password,
				formData : {
					uid : d.uid
				}
			}		
		},{});
  	}
    
 
	  
}

export default new AuthorizeManage()