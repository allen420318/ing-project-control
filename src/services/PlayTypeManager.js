import baseService from './base.js';


let ns = 'Basic';
let testApiHost = "http://"+location.host+":3000/";

class PlayTypeManager extends baseService{
  
   constructor() {
        super();
    }
  
  //取得玩法(棄用)
  getPlayTypeList(logintoken,uid){
	var url =  this.url;
	url = testApiHost+this.constructor.name+"/"+"getPlayTypeList";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'playModes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
  getPlayTypeDetail(logintoken,uid){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getPlayTypeDetail";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'playModes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
  
  updatePlayType(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"updatePlayTypeDetail";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'playModes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				uid : d.uid,
				title : d.title,
				alias : d.alias,
				code : d.code,
				locked : d.locked,
				betAreas : d.betAreas				
			}
		}		
	},{});
  }
  
  
  addPlayType(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"addPlayTypeDetail";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'playModes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData : {
				gameSetUid : d.gameSetUid,
				title : d.title,
				alias : d.alias,
				code : d.code,
				locked : d.locked,
				betAreas : d.betAreas				
			}
		}		
	},{});
  }
  
  

}

export default new PlayTypeManager()