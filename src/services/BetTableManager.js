import baseService from './base.js';


let ns = 'GameManagement';
let testApiHost = "http://"+location.host+":3000/";

class BetTableManager extends baseService{
  
   constructor() {
        super();
    }
  
 
   //新增賭桌
  addTable(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData : {
				titles : d.titles,
				themeUid :d.themeUid,
				gameSetUid : d.gameSetUid,
				tableSpeciesUid : d.tableSpeciesUid,
				casinoLocationUid : d.casinoLocationUid,
				systemCode : d.systemCode,
				frontCode : d.frontCode,
				siteCode : d.siteCode,
				betTime : d.betTime,
				settleTime : d.settleTime,
				peek1Time : d.peek1Time,
				peek2Time : d.peek2Time
			}
		}
	},{});
  }
  
  
   //刪除賭桌
  delTable(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			password : d.password,
			formData : {
				uidList : d.uidList				
			}
		}
	},{});
  }
  
  
  
     //修改賭桌
  modifyTable(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				uid : d.uid,
				titles : d.titles,
				frontCode: d.frontCode,
				siteCode: d.siteCode
			}
		}
	},{});
  }
  
  
  
  
  
     //賭桌列表
  getTableList(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'tableSets',
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex
				},
				conditions:{
					filters : {
						themeUid : d.themeUid,
						gameSetUid : d.gameSetUid,
						tableSpeciesUid : d.tableSpeciesUid
					}
				}
				
			}
		}
	},{});
  }
  
  
    //賭桌明細
  getTableDetail(logintoken,uid){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				uid : uid
			}
		}
	},{});
  }
  
  
     //停用賭桌
  lockTable(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'lock',
			password : d.password,
			formData : {
				uidList : d.uidList				
			}
		}
	},{});
  }
  
  
    //啟用賭桌
  unlockTable(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'unlock',
			password : d.password,
			formData : {
				uidList : d.uidList				
			}
		}
	},{});
  }
  
  
  
  
}

export default new BetTableManager()