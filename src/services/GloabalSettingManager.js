import baseService from './base.js';

let ns = 'global';
class GloabalSettingManager extends baseService{
  
   constructor() {
		super();
   }

	
  
  
   version(){

	var url = "/version.txt"+"?v="+_.random(1, 1000,true);

	return this.$http.get(url,{
		transformResponse	: [
			function(data){				
				var v = data.trim();
				var re = {
					exitCode : true,
					data : {
						version : v
					}					
				}
				return re;
			}
		],
		headers:{
			'Accept': 'text/plain'
		},
		customError : true
	});
  }
  
  
  
   
}

export default new GloabalSettingManager()