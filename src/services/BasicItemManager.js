import baseService from './base.js';

let ns = 'Assist';
class BasicItemManager extends baseService{
  
   constructor() {
        super();
        this.basicItemClass = [
			{
				alias:'gameVendor',
				name :'平台'
			},
			{
				alias:'tableTypes',
				name :'桌種'				
			}			
		];
		
		this.basicItem = {
			CL :'casinoLocations',  		//現場位置
			GRS : 'gameResultState', 		//遊戲紀錄/投注紀錄狀態
			TFT: 'transferTypes'
		};
    }
  
  
  



 //取得基本設定
  gameBasicSetting(logintoken,d){
	var _this = this;	  
	var optionTypes = {};
	
	_.each(d,function(v,k){
		
		optionTypes[v] = {modeSet : []};
	});
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'basicOptions',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'list',
			formData : {
				optionTypes : optionTypes
			}
		}		
	},{});
  }
		
  
	
	
	
}

export default new BasicItemManager()