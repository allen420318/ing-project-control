import baseService from './base.js';


let ns = 'Basic';
let testApiHost = "http://"+location.hostname+":3000/";

class TableTypeManager extends baseService{
  
   constructor() {
        super();
    }
  
  //取得桌種列表
  getTableTypeList(logintoken){
	var url =  this.url;
	// url = testApiHost+this.constructor.name+"/"+"getTableTypeList";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSpecies',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'tableSpecies'				
			}			
		}		
	},{});
  }
  
  
  //取得桌種屬性列表
  getTableProperties(logintoken){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getTableProperties";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableProperties',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'tableProperties'				
			}			
		}		
	},{});
  }
  
  
  //更新桌種
  updateTableType(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"updateTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSpecies',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : d
		}		
	},{});
  }
  
  
  //刪除桌種
  delTableType(logintoken,uid){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSpecies',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
   //新增桌種
  addTableType(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delTableType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'tableSpecies',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData : {
				title : d.title,
				alias :d.alias,
				code : d.code,
				properties : d.properties
			}
		}		
	},{});
  }
  
  
}

export default new TableTypeManager()