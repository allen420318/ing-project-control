import baseService from './base.js';


let ns = 'Basic';
let testApiHost = "http://"+location.host+":3000/";

class GameTypeManager extends baseService{
  
   constructor() {
        super();
    }
  
  //取得遊戲列表
  getGameTypeList(logintoken){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getGameTypeList";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'gameSets'				
			}			
		}		
	},{});
  }
  
  
  //取得遊戲詳細資料
  getGameTypeDetail(logintoken,uid){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getGameTypeDetail";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				uid : uid
			}			
		}		
	},{});
  }
  
  
  //更新遊戲
  updateGameType(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"updateGameType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				uid : d.uid,
				tableSpecies : d.tableSpecies	
			}
		}		
	},{});
  }
  
  
  //更新遊戲排序
  updateGameSort(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"updateGameSort";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'orderSet',
			formData : d
		}		
	},{});
  }
  
  
  //刪除遊戲
  delGameType(logintoken,uid){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"delGameType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
   //新增遊戲
  addGameType(logintoken,d){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"addGameType";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData : {
				title : d.title,
				alias :d.alias,
				code : d.code,
				tableSpecies : d.tableSpecies,
				playMode : d.playMode
			}
		}		
	},{});
  }
  
  //取得遊戲與桌種對應關係
   getGameTableRelation(logintoken){
	var url =  this.url;
	//url = testApiHost+this.constructor.name+"/"+"getGameTableRelation";
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'list'
		}		
	},{});
  }
  
  
  
  
  
}

export default new GameTypeManager()