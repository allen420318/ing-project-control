import axios from "axios"
import { MessageBox } from "element-ui"

const _Setting = require(`../configs/${__BuildVar__.webType}/meta.js`).default
var _errorMessage = require("../libs/errorMessage.js")

export default class baseService {
  constructor() {
    var _this = this
    var apiSetting = _Setting.apiSettings

    this.url = apiSetting.endpoint
    this.defaultHeader = apiSetting.headers
    this.emptySub = "DataEmpty" //無資料
    this.noLoginSub = "LoginToeknInvalid" //logintoken失效

    this.$http = axios.create({
      baseUrl: "",
      headers: apiSetting.headers,
      timeout: apiSetting.timeout,
      transformRequest: [
        function(data) {
          return JSON.stringify(data)
        }
      ],
      transformResponse: [
        function(data) {
          var re
          try {
            re = _.cloneDeep(JSON.parse(data))

            if (!re.exitCode) {
              var t = JSON.parse(re.data.message).sub
            }

            if (_this.emptySub == t) {
              re.data = {
                rows: [],
                pagination: {
                  pageCount: 0,
                  pageIndex: 0
                }
              }

              re.exitCode = true
            }
            if (_this.noLoginSub == t) {
              localStorage.clear()
              location.reload()
            }
          } catch (err) {
            re = {
              exitCode: false,
              msg: "Not JSON data",
              data: data
            }
          }

          return re
        }
      ]
    })

    this.$http.interceptors.request.use(
      function(config) {
        return config
      },
      function(error) {
        //请求错误时做些事

        return Promise.reject(error)
      }
    )

    var _this = this
    this.$http.interceptors.response.use(
      function(data) {
        //console.log(data);
        var response = data.data
        var _config = data.config
        var customError = _config.customError || false

        if (!response.exitCode) {
          if (customError) {
            throw response.data
          } else {
            console.log("Global error")
            _this.handleError(response.data)
            throw response.data
          }
        }

        if (typeof response.data.executeInfo != "undefined") {
          if (typeof response.data.executeInfo.fail != "undefined") {
            if (_.isPlainObject(response.data.executeInfo.fail)) {
              if (customError) {
                throw response.data
              } else {
                _this.handleError(response.data)
                throw response.data
              }
            }
          }
        }

        return response.data
      },
      function(error) {
        //系統錯誤
        //Message.error({message: error.response.statusText+'('+error.response.status+')'});
        //_this.handleError(error.response.statusText+'('+error.response.status+')');

        return Promise.resolve(error.response)
      }
    )
  }

  handleError(d) {
    if (d.class === "Exception") {
      var _textMessage = _errorMessage.textMessage
      var _t = JSON.parse(d.message).sub
      for (var i = 0; i < _textMessage.length; i++) {
        if (_t == _textMessage[i].sub) {
          var _errorMsg = _textMessage[i].msg
        }
      }

      if (_t === "PermissionNotExists") {
        window.location.reload()
      } else {
        MessageBox.alert(_errorMsg, {
          showClose: false,
          customClass: "_c_MsgBox err1",
          showCancelButton: false,
          confirmButtonText: "確定",
          closeOnPressEscape: false,
          center: true
        })
      }
    } else {
      MessageBox.alert("內容錯誤", {
        showClose: false,
        customClass: "_c_MsgBox err1",
        showCancelButton: false,
        confirmButtonText: "確定",
        closeOnPressEscape: false,
        center: true
      })
    }

  }
}
