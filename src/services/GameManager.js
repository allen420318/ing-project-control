import baseService from './base.js';

let ns = 'gameManagement';
class GameManager extends baseService{
  
   constructor() {
        super();
        this.basicItemClass = [
			{
				alias:'gameVendor',
				name :'平台'
			},
			{
				alias:'tableTypes',
				name :'桌種'				
			}			
		];
		
		this.basicItem = [
			'gameTypes',  		//遊戲類型
			'gameVendor', 		//遊戲平台
			'tableTypes', 		//桌種
			'serverTypes', 		//伺服器類型
			'serverSettings', 	//伺服器設定
			'casinoLocations',	//現場位置
			'themes'			//廳館列表
			];
    }
  
  
  
  //取得基本項目
  getBasicItems(logintoken,dataTypeList){	
	
	var dataTypeList = dataTypeList || this.basicItem;
	return this.$http.post(this.url,{
		ns : ns,
		work : 'basicItems',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			dataTypes:dataTypeList
		}		
	},{});
  }
  
  
  /*
  getCasinoLocations(logintoken){	  
	  return this.getBasicItems(logintoken,["casinoLocations"]);
  }
  */
  
  //刪除遊戲平台
  gameVendor_del(logintoken,uid){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameVendor',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command: 'delete',
			formData : {
				uidList: [uid]
			}
		}		
	},{});
  }
  
  //新增平台
  gameVendor_add(logintoken,title){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameVendor',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command: 'add',
			formData : {
				title : title
			}
		}		
	},{});
  }
  
  //現場位置 - 刪除
  casinoLocations_del(logintoken,uid){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'casinoLocations',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command: 'delete',
			formData : {
				uidList: [uid]
			}
		}		
	},{});
  }
  
  
  //現場位置 - 新增
  casinoLocations_add(logintoken,gameVendorUid,title,code){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'casinoLocations',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command: 'add',
			formData : {
				gameVendorUid : gameVendorUid,
				title : title,
				code : code
			}
		}		
	},{});
  }
  
	////伺服器 - 刪除
    serverSettings_del(logintoken,uid){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'serverSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command: 'delete',
			formData : {
				uidList: [uid]
			}
		}		
	},{});
  }
  
  //伺服器 - 新增
   serverSettings_add(logintoken,d){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'serverSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command: 'add',
			formData : {
				serverTypesUid: d.serverTypesUid,
				alias: d.alias,
				address : d.address,
				remark : d.remark
			}
		}		
	},{});
  }
  
  
   //取得廳館列表
  getHallList(logintoken){	
		
	return this.$http.post(
		this.url,
		{
			ns : ns,
			work : 'basicItems',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				dataTypes:["themes"]
			}	
		},{
			transformResponse:[
				function(data){				
					var re; 
					try{
						re = JSON.parse(data);						
						re.data = re.data.themes;						
						}
					catch(err){
						re = {
							exitCode : false,
							msg : 'Not JSON data',
							data : data
							};
						}
					
					return re;		
					
				}
			]		
	});
  }
  
  //取得廳館列表 v2
  getThemes(logintoken,d){
	  	var _this = this;
		
		
		return this.$http.post(this.url,{
			ns : ns,
			work : 'themes',
			version : '0.0.1',
			loginToken:logintoken,
			data : {
				command : 'report',
				formData : {
					name : 'themes',
					pagination : {
						pageSize : d.pageSize,
						pageIndex : d.pageIndex
					},
					conditions:{
						filters:{
							casinoLocations : d.casinoLocations			
						}			
					}				
				}
			}		
		},{		
			/*
			transformResponse:[
					function(data){				
						var re; 
						try{
							re = JSON.parse(data);
							var t = _.trim(re.data.class);
							
							if(_this.emptyClass.indexOf(t) != -1)				
								{									
									re.data.rows = [];
									re.data.pagination = {
										pageCount : 0,
										pageIndex : 0									
									}
									re.exitCode = true;
								}
							else
								{
									re.data = {
											pagination : re.data.executeInfo.pagination,
											rows : re.data.executeInfo.rows									
									}
								}
							
							}
						catch(err){
							re = {
								exitCode : false,
								msg : 'Not JSON data',
								data : data
								};
							}
						
						return re;		
						
					}
				]
				*/
			
		});
    
  }
  
  
  
  
  
  
  
  //新增廳館
  getHall_add(logintoken,d){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'themes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				title : d.title,
				themeTypeId:d.isRobotArm === true ? 1 : 0,
				sorting : d.sorting				
			}
		}		
	},{});
  }
  
  //編輯廳館
  getHall_modify(logintoken,d){	
		
	return this.$http.post(this.url,{
		ns : ns,
		work : 'themes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				title : d.title,
				uid : d.uid
			}
		}		
	},{});
  }
  
  //編輯廳館排序
  getHall_orderSet(logintoken,d){	
		
	return this.$http.post(this.url,{
		ns : ns,
		work : 'themes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'orderSet',
			formData : d
		}		
	},{});
  }
  
  //刪除廳館
  getHall_del(logintoken,password,uidList){
		
	return this.$http.post(this.url,{
		ns : ns,
		work : 'themes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			password : password,
			formData : {
				uidList:uidList
			}
		}		
	},{});
  }
  
  
  //鎖定廳館
  getHall_lock(logintoken,password,uidList){	
		
	return this.$http.post(this.url,{
		ns : ns,
		work : 'themes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'lock',
			password : password,
			formData : {
				uidList:uidList
			}
		}		
	},{});
  }
  
  
  //廳館解鎖
  getHall_unlock(logintoken,password,uidList){	
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'themes',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'unlock',
			password : password,
			formData : {
				uidList:uidList
			}
		}		
	},{});
  }
  


  
  //賭桌列表
  betTable_list(logintoken,d){
	
	var _this = this;
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSets',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'tableSets',
				pagination : {
					pageSize:d.pageSize || 20,
					pageIndex:d.pageIndex || 0
				},
				conditions:{
					filters:{
						themeUid : d.themes  || [],
						gameSetUid: d.gameTypes || [],
						tableSpeciesUid : d.tableTypes  || []
					}					
				}
			}
		}		
	},{
		/*
		transformResponse:[
				function(data){				
					var re; 
					try{
						re = JSON.parse(data);
						var t = _.trim(re.data.class);
						
						switch(true)
						{
							case (_this.emptyClass.indexOf(t) != -1):
								re.data = {
									rows : []
								};
								re.exitCode = true;
							break;
							
							case(t == ""):
								re.data = re.data.executeInfo;
							break;
							
							default:
							
							break;
							
						}
					}
					catch(err){
						re = {
							exitCode : false,
							msg : 'Not JSON data',
							data : data
							};
						}
					
					return re;		
					
				}
			],
		*/	
		customError:false	
		
	});
  }	
  

  
  //新增賭桌
  betTable_add(logintoken,d){
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData : {
				titles : {
					'EN-US': d.titles['en-us'],
					'ZH-CN': d.titles['zh-cn'],
					'ZH-TW': d.titles['zh-tw']
				},
				themeUid:d.themeUid,
				gameTypeUid:d.gameTypeUid,
				tableTypeUid:d.tableTypeUid,
				tableId:d.tableId,
				casinoTableId : d.casinoTableId,
				videoDelayTime : d.videoDelayTime,
				betTime : d.betTime,
				settleTime : d.settleTime,
				peek1Time : d.peek1Time,
				peek2Time : d.peek2Time,
				isPairTableSeat : d.isPairTableSeat,
				limits: d.limits
			}
		}		
	},{});
  }	
  
  
  //修改賭桌
  betTable_modify(logintoken,d){
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				uid : d.uid,
				titles : {
					'EN-US': d.titles['en-us'],
					'ZH-CN': d.titles['zh-cn'],
					'ZH-TW': d.titles['zh-tw']
				},				
				gameTypeUid:d.gameTypeUid,
				tableTypeUid:d.tableTypeUid,				
				casinoTableId : d.casinoTableId,
				videoDelayTime : d.videoDelayTime,
				betTime : d.betTime,
				settleTime : d.settleTime,
				peek1Time : d.peek1Time,
				peek2Time : d.peek2Time,
				isPairTableSeat : d.isPairTableSeat,
				limits: d.limits
			}
		}		
	},{});
  }	
  
  
   //刪除賭桌
  betTable_del(logintoken,d){
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			password : d.password,
			formData : {
				uidList:d.uidList
			}
		}		
	},{});
  }	
  
  
   //鎖定賭桌
  betTable_lock(logintoken,d){
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'lock',
			password : d.password,
			formData : {
				uidList:d.uidList
			}
		}		
	},{customError:false});
  }	
  
  
   //解鎖賭桌
  betTable_unlock(logintoken,d){
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'unlock',
			password : d.password,	
			formData : {
				uidList:d.uidList
			}
		}
	},{customError:false});
  }	
  
  
  //賭桌詳細
  betTable_detail(logintoken,uid){
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'details',			
			formData : {
				uid:uid
			}
		}
	},{customError:false});
  }	
  
  
  
   //限紅列表
  tableLimits(logintoken,uidList){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'details',
			formData : {
				gameTypeUids:uidList
			}
		}		
	},{});
  }	
  
  
     //限紅列表第二版
  tableLimitsV2(logintoken,uidList){
	
	var odds = {
				BACCARAT:
				[
					{
						title : '賠率0.5-1 (大廳顯示限紅)',
						range : [0.5,1]
					},
					{
						title : '賠率1.5',
						range : 1.5
					},
					{
						title : '賠率8',
						range : 8
					},
					{
						title : '賠率11',
						range : 11
					},
				]
			};
	
	
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'details',
			formData : {
				gameTypeUids:uidList
			}
		}		
	},{
			transformResponse:[
				function(data){
					var re; 
					try{
						re = JSON.parse(data);						
						
						var tmp = _.cloneDeep(re.data.executeInfo);
						var d = [];
						var _odds  = _.cloneDeep(odds);
						_.forEach(tmp,function(val,key){
							var rows = val.tableLimits;
							var gameTypeAlias = val.gameTypeAlias;
							_.forEach(rows,function(v,k){
								rows[k].id = rows[k].id || (k+1);
								//tmp[key].oddsSettingUid = v.oddsSettingUid;
								var tmpLimit = [];
								_.forEach(v.limits,function(v1,k1){									
									_.forEach(odds[gameTypeAlias],function(v2,k2){
										tmpLimit[k2] = tmpLimit[k2] || {uid:[]};
										_odds[gameTypeAlias][k2].uid = _odds[gameTypeAlias][k2].uid || {}
										if(_.isArray(v2.range))
										{
											//if(_.includes(v2.range,v1.odds))
											if(v2.range[v2.range.length-1] == v1.odds)
												{
													tmpLimit[k2].min = v1.min;
													tmpLimit[k2].max = v1.max;
													tmpLimit[k2].uid.push(v1.oddsItemUid);
													_odds[gameTypeAlias][k2].uid[v1.oddsItemUid] = v1.oddsItemUid;													
												}
											
											if(_.inRange(v1.odds,v2.range[0],v2.range[1]))
												{
													tmpLimit[k2].min = v1.min;
													tmpLimit[k2].max = v1.max;
													tmpLimit[k2].uid.push(v1.oddsItemUid);
													_odds[gameTypeAlias][k2].uid[v1.oddsItemUid] = v1.oddsItemUid;
												}
										}
										else
										{
											if(v2.range == v1.odds)
												{
													tmpLimit[k2].min = v1.min;
													tmpLimit[k2].max = v1.max;
													tmpLimit[k2].uid.push(v1.oddsItemUid);
													_odds[gameTypeAlias][k2].uid[v1.oddsItemUid] = v1.oddsItemUid;
												}
										}										
									});
								});
								
								rows[k].limits = tmpLimit;
							});
							tmp[key].tableLimits = rows;
						});
						
						
						
						re.data = {};
						_.forEach(tmp,function(v,k){
							re.data[v.gameTypeUid] = {
									tableLimits : tmp[k].tableLimits,
									gameTypeAlias : tmp[k].gameTypeAlias,
									gameTypeTitle : tmp[k].gameTypeTitle,
									gameTypeUid : tmp[k].gameTypeUid,
									odds : _odds[v.gameTypeAlias],
									oddsSettingUid : tmp[k].oddsSettingUid
							}							
						});
					}
					catch(err){
						console.log(err);
						re = {
							exitCode : false,
							msg : 'Not JSON data',
							data : data
							};
						}
					
					return re;
				}
			]
	});
  }	
  
  
       //限紅列表第3版
  tableLimitsV3(logintoken,uidList){
	
	var oddsRange = {
				BACCARAT:
				[
					{
						title : '賠率0.5-1 (大廳顯示限紅)',
						range : [0.5,1]
					},
					{
						title : '賠率1.5',
						range : 1.5
					},
					{
						title : '賠率8',
						range : 8
					},
					{
						title : '賠率11',
						range : 11
					},
				]
			};
	
	var checkRange = this.checkRange;
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'details',
			formData : {
				gameTypeUids:uidList
			}
		}		
	},{
			transformResponse:[
				function(data){
					var re; 
					try{
						re = JSON.parse(data);
						var tmp = _.cloneDeep(re.data.executeInfo);
						var rows = {};
						
						
						_.forEach(tmp,function(v,k){
							var oddsSettingUid = '';	
							var gameTypeUid = v.gameTypeUid;
							var row = {
										gameTypeAlias : v.gameTypeAlias,
										gameTypeTitle : v.gameTypeTitle,
										gameTypeUid : v.gameTypeUid										
									};
							var _oddsRange  = _.cloneDeep(oddsRange[v.gameTypeAlias]);
							_.forEach(v.tableOdds,function(v1,k1){
								oddsSettingUid = v1.oddsSettingUid;								
								_.forEach(v1.odds,function(v2,k2){									
									
									_.forEach(_oddsRange,function(_v,_k){
										if(checkRange(v2.odds,_v.range))
										{
											_oddsRange[_k].uid = _oddsRange[_k].uid || {};
											_oddsRange[_k].uid[v2.oddsItemUid] = v2.oddsItemUid;
										}
									
									});
								});
								
								//因應目前特別狀態，只有一組資料
								return false;
							});
							
							
							var tmpLimit = [];
							//v.tableLimits = [];
							_.forEach(v.tableLimits,function(v1,k1){
								tmpLimit[k1] = {
									id : v1.id,
									maxTableLimit : v1.maxTableLimit,
									minTableAmount : v1.minTableAmount,
									tableLimitUid : v1.tableLimitUid,
									limits : []
								};
								var _limits = [];
								_.forEach(v1.limits,function(v2,k2){
									_.forEach(_oddsRange,function(_v,_k){
										_limits[_k]	 = _limits[_k] || {uid:[]};
										if(checkRange(v2.odds,_v.range))
										{											
											_limits[_k].min = v2.min;
											_limits[_k].max = v2.max;
											_limits[_k].uid.push(v2.oddsItemUid);
										}									
									});
								});	
								tmpLimit[k1].limits = _limits;
								
							});	
							
							row.oddsSettingUid = oddsSettingUid;
							row.tableLimits = tmpLimit;
							row.odds = _oddsRange;
							rows[gameTypeUid] = _.cloneDeep(row);

							
						});
						
						
						re.data = rows;
						
						
						
					}
					catch(err){
						console.log(err);
						re = {
							exitCode : false,
							msg : 'Not JSON data',
							data : data
							};
						}
					
					return re;
				}
			]
	});
  }	
  
  
     //更新限紅列表
  updateTableLimits(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			password : d.password,	
			formData : {
				uid : d.uid,
				oddsSettingUid : d.oddsSettingUid,
				minTableAmount : d.minTableAmount,
				maxTableLimit : d.maxTableLimit,
				id : d.id,
				limits : d.limits				
			}
		}		
	},{});
  }	
  
  
   //刪除限紅
  DelTableLimits(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'delete',
			password : d.password,	
			formData : {
				uidList : d.uid				
			}
		}		
	},{});
  }	
  
   //新增
   addTableLimits(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableLimits',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			password : d.password,	
			formData : {
				gameTypeUid : d.gameTypeUid,
				oddsSettingUid : d.oddsSettingUid,
				minTableAmount : d.minTableAmount,
				maxTableLimit : d.maxTableLimit,
				id : d.id,
				limits : d.limits				
			}
		}		
	},{});
  }	
  
  
  
  
  //取得遊戲紀錄
  gameRecords(logintoken,d){
	var _this = this;	  
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'baccaratGameResult',
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex
				},
				conditions:{
					filters:{
						themes : d.themes,
						gameTypes : d.gameTypes || [],
						tableTypes : d.tableTypes || []						
					},
					where:{
						roundTime:d.roundTime,
						tableId : d.tableId,
						shoeNo : d.shoeNo,
						roundNo : d.roundNo
					}					
				}				
			}
		}		
	},{		
		/*
		transformResponse:[
				function(data){				
					var re; 
					try{
						re = JSON.parse(data);
						var t = _.trim(re.data.class);
						if(_this.emptyClass.indexOf(t) != -1)				
							{									
								re.data.rows = [];
								re.data.pagination = {
									pageCount : 0,
									pageIndex : 0									
								}
								re.exitCode = true;
							}
						else
							{
								re.data = {
									pagination : re.data.executeInfo.pagination,
									rows : re.data.executeInfo.rows									
								}
							}
						
						}
					catch(err){
						re = {
							exitCode : false,
							msg : 'Not JSON data',
							data : data
							};
						}
					
					return re;		
					
				}
			]*/
		
	});
  }
  
  
   //修改賽果
  modifyGameRecords(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			password : d.password,
			formData : {
				//gameTypeUid : d.gameTypeUid,
				result : d.result,
				reason : d.reason,
				uid : d.uid				
			}
		}		
	},{});
  }	
  
  //退押注
  returnBet(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'returnBet',
			password : d.password,
			formData : {
				//gameTypeUid : d.gameTypeUid,				
				reason : d.reason,
				uid : d.uid
				//password:d.password
			}
		}		
	},{});
  }	
  
  
  
   //get 詳細的遊戲紀錄
  getDetailGameRecords(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'details',
			formData : {
				gameTypeUid : d.gameTypeUid,				
				uid : d.uid				
			}
		}		
	},{});
  }	
  
  
  
  checkRange(val,range){
	  
	  var re = false;
	  if(_.isArray(range))
		{											
			if(range[range.length-1] == val)
				{
					re = true;
				}

			if(_.inRange(val,range[0],range[1]))
				{
					re = true;
				}
		}
	else
		{
			if(range == val)
			{
				re = true;
			}
		}
	  
		return re;
	}
	
	
	
  //取得遊戲紀錄 : ByTime
  gameRecordsByTime(logintoken,d){
	var _this = this;	  
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'gameResultByDateTime',
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex
				},
				conditions:{
					filters:{
						themes : d.themes,
						gameSets : d.gameSets || [],
						tableSpecies : d.tableSpecies || []						
					},
					where:{
						roundTime:d.roundTime						
					}					
				}				
			}
		}		
	},{});
  }
  
  
  //取得遊戲紀錄 : byGameRound
  gameRecordsbyGameRound(logintoken,d){
	var _this = this;	  
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'gameResultByRound',
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex
				},
				conditions:{
					filters:{
						themes : d.themes						
					},
					where:{						
						frontCode : d.frontCode,
						shoeNo : d.shoeNo,
						roundNo : d.roundNo
					}					
				}				
			}
		}		
	},{});
  }
	
	
  //取得遊戲紀錄
  gameRecordsV2(logintoken,d,searchItem){
	
	var re;
	switch(searchItem)
	{
		
		case ("gameResultByDateTime"):
			re = this.gameRecordsByTime(logintoken,d);	
		break;
		
		case ("gameResultByRound"):
			re = this.gameRecordsbyGameRound(logintoken,d);	
		break;
	}
	
	return re;
	
  }


 //取得遊戲詳細資料
  gameResultDetail(logintoken,d){
	var _this = this;	  
	return this.$http.post(this.url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				uid : d.uid		
			}
		}		
	},{});
  }
		
  
	
	
	
}

export default new GameManager()