import baseService from "./base.js"

let ns = "main"
class UserManager extends baseService {
  constructor() {
    super()
    //this.selfParam = 'X';
  }

  // return data:[] 就是失效
  loginTokenIsAlive(loginToken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "noop",
        version: "0.0.1",
        loginToken: loginToken
      },
      {}
    )
  }

  login(d) {
    var tz = new Date().getTimezoneOffset()
    tz = 0 - tz
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "login",
        version: "0.0.1",
        data: {
          id: d.id,
          password: d.pwd,
          tz: tz
        }
      },
      {
        transformResponse: [
          //目前loginToken失效 跟 登入失敗的 例外一樣
          //為了不跑loginToken 失效的程序，需自訂transformResponse，
          //pass過 Global 的 transformResponse
          function(data) {
            var re = JSON.parse(data)
            return re
          }
        ]
      }
    )
  }

  logout(loginToken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "logout",
        version: "0.0.1",
        loginToken: loginToken
      },
      {}
    )
  }

  apply4forgetpwd(email) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "forgetPassword",
        version: "0.0.1",
        data: {
          email: email
        }
      },
      {}
    )
  }

  VerifyApply4forgetpwdToken(token) {
    return this.$http.post(
      this.url,
      {
        ns: "main",
        work: "forgetPasswordTokenVerify",
        version: "0.0.1",
        data: {
          token: token
        }
      },
      { customError: true }
    )
  }

  resetPwd(pwd, Token) {
    return this.$http.post(this.url, {
      ns: "main",
      work: "changePasswordByToken",
      version: "0.0.1",
      data: {
        password: pwd,
        token: Token
      }
    })
  }

  forceResetPwd(d) {
    return this.$http.post(this.url, {
      ns: "main",
      work: "forceChangePassword",
      version: "0.0.1",
      loginToken: d.loginToken,
      data: {
        password: d.pwd
      }
    })
  }

  changePwd(oldPwd, newPwd, Token) {
    return this.$http.post(this.url, {
      ns: "main",
      work: "changePassword",
      loginToken: Token,
      version: "0.0.1",
      data: {
        password: oldPwd,
        formData: {
          password: newPwd
        }
      }
    })
  }

  //取得權限選單
  async getPermissionMenu(logintoken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "Permission",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "list"
        }
      },
      {}
    )
  }

  //權限重新讀取
  reloadPermission(logintoken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "Permission",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "reload"
        }
      },
      {}
    )
  }
}

export default new UserManager()
