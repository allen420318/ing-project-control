import baseService from './base.js';

let ns = 'systemManage';
class AccountManager extends baseService{
  
   constructor() {
        super();
        //this.selfParam = 'X';
    }
  
  //取得後台使用者列表
  getAccountList(logintoken,account=null){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'list',
			formData : {
				account : account				
			}			
		}
	},{});
  }

  //取得後台使用者詳細資料
  getAccountDetail(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'detail',
			formData : {
				uid : uid				
			}			
		}
	},{});
  }

  //新增後台使用者
  addAccount(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'add',
			formData : {
				account : d.account,
				nickname : d.nickname,
				password : d.password,
				email : d.email,
				permissionList : d.permissionList
			}			
		}
	},{});
  }

  //更新後台使用者
  updateAccount(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'modify',
			formData : {
				uid : d.uid,
				nickname : d.nickname,				
				email : d.email,
				permissionList : d.permissionList
			}			
		}
	},{});
  }

  //刪除後台使用者
  delAccount(logintoken,uid,password){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'delete',
			password : password,
			formData : {
				uid : uid
			}			
		}
	},{});
  }

  //lock後台使用者
  LockAccount(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'lock',
			formData : {
				uid : uid,				
			}			
		}
	},{});  
  }

  //unlock後台使用者
  unLockAccount(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'controller',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command :'unlock',
			formData : {
				uid : uid				
			}			
		}
	},{});
  }

  

}

export default new AccountManager()