import baseService from './base.js';

let ns = 'agentFeatures';
class AgentManager extends baseService{
  
   constructor() {
        super();
        //this.selfParam = 'X';
    }
  
  getTopAgentList(logintoken,account=''){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'getTopAgentList',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			account:account
		}		
	},{});
  }
  
  
  getTopAgentListV1(logintoken,d){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'topAgent',
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex					
				},
				conditions : {
					filters : [],
					where : {
						account : {
							account : d.account
						}
					}	
				}
				
			}
		}		
	},{});
  }
  
  getTopAgent(logintoken,uid=''){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'getTopAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			uid : uid
		}		
	},{});
  }
  
  queryReportPermissionAccount(logintoken,account=''){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'getReportPermission',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			account:account
		}		
	},{customError:true});
  }
  

  addAgent(logintoken,d){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'add',
			formData:{
				account: d.account,
				nickname : d.nickname,
				password : d.password,
				code : d.code,
				areaId : d.areaId,
				mobile : d.mobile,
				email : d.email,
				walletId : d.walletId,
				apiUrl: d.apiUrl,
				reportUids : d.reportUids

			}
		}		
	},{customError:false});
  }
  
  
    updateAgent(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData:{
				uid: d.uid,
				nickname : d.nickname,				
				areaId : d.areaId,
				mobile : d.mobile,
				email : d.email,
				walletId : d.walletId,
				apiUrl : d.apiUrl,
				apiSignKey : d.apiSignKey,
				reportUids : d.reportUids				
			}
		}		
	},{customError:false});
  }
  
	  
  	createSignKey(logintoken){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data:{
			command:'randmonSign'
		}
	},{});
  }


    getAreaList(logintoken,account=''){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'getAreaList',
		version : '0.0.1',
		loginToken:logintoken
	},{});
  }
  
	getAgentKey(logintoken,groupUid){	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'getAgentKey',
		version : '0.0.1',
		loginToken:logintoken,
		data:{
			groupUid:groupUid
		}
	},{});
  }

	CreateAgentKey(logintoken,groupUid,password){	
		return this.$http.post(this.url,{
			ns : ns,
			work : 'agentKey',
			version : '0.0.1',
			loginToken:logintoken,
			data:{
				command :"generate",
				password : password,
				formData:{
					uid:groupUid	
				}			
			}
		},{});
  }
  
  //啟用
  toEnable(logintoken,d){
	  
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'enable',
			password : d.password,
			formData:{
				uid: d.uid,
			}
		}		
	},{});
  }
  
  
  //停用
  toDisable(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'disable',
			password : d.password,
			formData:{
				uid: d.uid
			}
		}		
	},{});
  }
  
  
  //解鎖
  toUnLock(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'topAgent',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'unlock',
			formData:{
				uid: d.uid
			}
		}		
	},{});
  }
  
  
    //金鑰啟用
  apiToEnable(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'agentKey',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'enable',
			password : d.password,
			formData:{
				uid: d.uid
			}
		}		
	},{});
  }
  
    //金鑰停用
  apiToDisable(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'agentKey',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'disable',
			password : d.password,
			formData:{
				uid: d.uid
			}
		}		
	},{});
  }
  
  
   //匯出金鑰(目前沒有使用)	
   exportfile(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'agentKey',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'export',			
			formData:{
				uid: d.uid
			}
		}		
	},{});
  }
    
}

export default new AgentManager()