import baseService from "./base.js"
import { getAPIFormatTimeString} from "@/libs/time.js"

let ns = "systemManage"
class MaintainManager extends baseService {
  constructor() {
    super()
    //this.selfParam = 'X';
  }

  //取得系統維護列表
  getList(logintoken, name) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "scheduleList",
          formData: {
            name: name
          }
        }
      },
      {}
    )
  }

  //取得維護種類列表
  getTypeList(logintoken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "getMaintainType",
        version: "0.0.1",
        loginToken: logintoken
      },
      {}
    )
  }

  //取得維護狀態列表
  getStatusList(logintoken) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "getMaintainStatus",
        version: "0.0.1",
        loginToken: logintoken
      },
      {}
    )
  }

  //申請維護項目
  toApply(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "add",
          formData: {
            typeId: d.typeId,
            noticeTime: d.noticeTime,
            startTime: getAPIFormatTimeString(d.startTime),
            reason: d.reason,
            targetData: d.targetData
          }
        }
      },
      {}
    )
  }

  //結束維護項目
  doComplete(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "finish",
          password: d.password,
          formData: {
            uid: d.uid
          }
        }
      },
      {}
    )
  }

  //重載維護項目
  doRefresh(logintoken, uid) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "reload",
          formData: {
            uid: uid
          }
        }
      },
      {}
    )
  }

  //修改維護項目的時間
  doUpdate(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "modify",
          password: d.password,
          formData: {
            uid: d.uid,
            startTime: d.startTime
          }
        }
      },
      {}
    )
  }

  //刪除維護項目
  doDel(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "delete",
          password: d.password,
          formData: {
            uid: d.uid
          }
        }
      },
      {}
    )
  }

  //取得系統維護記錄
  getRecord(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "report",
          formData: {
            name: "MaintainsBySystem",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              filters: {
                typeId: d.typeId
              },
              where: {
                roundTime: {
                  start: getAPIFormatTimeString(d.start),
                  end: getAPIFormatTimeString(d.end)
                }
              }
            }
          }
        }
      },
      {}
    )
  }

  //取得代理維護記錄
  getAgentRecord(logintoken, d) {
    return this.$http.post(
      this.url,
      {
        ns: ns,
        work: "maintains",
        version: "0.0.1",
        loginToken: logintoken,
        data: {
          command: "report",
          formData: {
            name: "MaintainsByAgent",
            pagination: {
              pageSize: d.pageSize,
              pageIndex: d.pageIndex
            },
            conditions: {
              where: {
                roundTime: {
                  start: getAPIFormatTimeString(d.start),
                  end: getAPIFormatTimeString(d.end)
                }
              }
            }
          }
        }
      },
      {}
    )
  }

  //結束維護項目
  doCancel(uid) {
    return this.$http.post(
      this.url,
      {
        ns: 'maintain',
        work: "finish",
        version: "0.0.1",
        data: {
            uid: uid   
        }
      },
      {}
    )
	}
	
}

export default new MaintainManager()
