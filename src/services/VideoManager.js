import baseService from './base.js';

let ns = 'videoManagement';
class VideoManager extends baseService{
  
   constructor() {
        super();
        //this.selfParam = 'X';
    }
  
  //取得CDN 列表
  getCdnList(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'cdnSettings',			
				conditions:{
					filters:{
						casinoLocations : uidList						
					}
					
				}
			}			
		}		
	},{});
  }
  
  
  //新增CDN 項目
  addCdn(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'add',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				channelId : d.channelId,
				address : d.address,
				remark : d.remark				
			}
			
		}		
	},{});
  }
  
  
  //修改CDN 項目
   updateCdn(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'modify',
			formData : {
				uid : d.uid,
				channelId : d.channelId,
				address : d.address,
				remark : d.remark				
			}
			
		}		
	},{});
  }
  
  
  //設定預設CDN項目
  DefaultCdn(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'defaultOption',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
  
  //設定停用CDN項目
  DisableCdn(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'lock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  //設定啟用CDN項目
  EnableCdn(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'unlock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
  //刪除CDN 項目
 delCdn(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'delete',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
   //取得解析度 列表
  getResolutionList(logintoken,uidList,videoReceiverList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'cdnSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'videoResolution',			
				conditions:{
					filters:{
						casinoLocations : uidList,
						videoReceiver : videoReceiverList
					}
					
				}
			}
			
		}		
	},{});
  }
  
  
   //新增解析度 項目
  addResolution(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoResolution',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'add',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				resolution : d.resolution,
				sorting : d.sorting,
				remark : d.remark,
				videoReceiverUid :d.videoReceiverUid		
			}
			
		}		
	},{});
  }
  
  
  //修改解析度 項目
   updateResolution(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoResolution',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'modify',
			formData : {
				uid : d.uid,				
				resolution : d.resolution,
				sorting : d.sorting,
				remark : d.remark				
			}
			
		}		
	},{});
  }
  
  
    //預設解析度項目
  DefaultResolution(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoResolution',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'defaultOption',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
  
  //停用解析度項目
  DisableResolution(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoResolution',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'lock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  //啟用解析度項目
  EnableResolution(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoResolution',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'unlock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
  //刪除解析度 項目
 delResolution(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoResolution',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'delete',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
   //取得賭桌視訊  列表
  getTableVideoList(logintoken,uidList,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableVideo',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'tableVideo',			
				conditions:{
					filters:{
						casinoLocations : uidList						
					}					
				},
				pagination:{
					pageSize : d.pageSize || 100,
					pageIndex : d.pageIndex || 0
				}
			}
			
		}		
	},{});
  }
  
  
   //更新賭桌視訊
  updateTableVideo(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableVideo',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				uid:d.uid,
				cdnSettingUid : d.cdnSettingUid,
				appName : d.appName,
				streamName : d.streamName
			}
			
		}		
	},{});
  }
  
  
  //取得錄影視訊  列表
  getVideoRecord(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'videoRecord',				
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex
				},
				conditions:{
					filters:{
						casinoLocations : d.casinoLocationUid
						
					}					
				}
			}
			
		}		
	},{});
  }
  
  
    //新增錄影視訊 項目
  addVideoRecord(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'add',
			formData : {
				casinoLocationUid : d.casinoLocationUid,				
				address : d.address,
				remark : d.remark
			}
			
		}		
	},{});
  }
  
  
  //修改錄影視訊 項目
   updateVideoRecord(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'modify',
			formData : {
				uid : d.uid,
				address : d.address,
				remark : d.remark,
				casinoLocationUid : d.casinoLocationUid
			}
			
		}		
	},{});
  }
  
  
    //預設錄影視訊項目
  DefaultVideoRecord(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'defaultOption',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
  
  //停用錄影視訊項目
  DisableVideoRecord(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'lock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  //啟用錄影視訊項目
  EnableVideoRecord(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'unlock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
  //刪除錄影視訊 項目
 delVideoRecord(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoRecord',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'delete',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
  //取得播放視訊detail
  getVideoPlay(logintoken,casinoLocationUid){
	console.log("casinoLocationUid : ",casinoLocationUid)	;
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPlaySettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'details',
			formData : {
				casinoLocationUid : casinoLocationUid				
			}
			
		}		
	},{});
  }
  
  //更新播放視訊
  updateVideoPlay(logintoken,d){
	
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPlaySettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				address : d.address,
				appName : d.appName,
				format : d.format 
			}
			
		}		
	},{});
  }
  
  
  
  
  
  
  
   //取得全景  列表
  getAllView(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'videoPanoramaSettings',				
				conditions:{
					filters:{
						casinoLocations : d.casinoLocationUid
						
					}					
				}
			}
			
		}		
	},{});
  }
  
  
    //新增全景 項目
  addAllView(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'add',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				titles:{
					'EN-US' : d.titles["EN-US"],
					'ZH-TW' : d.titles["ZH-TW"],
					'ZH-CN' : d.titles["ZH-CN"]
				},
				address : d.address,
				appName : d.appName,
				streamName : d.streamName,
				sorting : d.sorting
			}
			
		}		
	},{});
  }
  
  
  //修改全景 項目
   updateAllView(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'modify',
			formData : {
				uid : d.uid,				
				casinoLocationUid : d.casinoLocationUid,
				titles:{
					'EN-US' : d.titles["EN-US"],
					'ZH-TW' : d.titles["ZH-TW"],
					'ZH-CN' : d.titles["ZH-CN"]
				},
				address : d.address,
				appName : d.appName,
				streamName : d.streamName,
				sorting : d.sorting
			}
			
		}		
	},{});
  }
  
  
    //預設全景項目
  DefaultAllView(logintoken,uid){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'defaultOption',
			formData : {
				uid : uid
			}
		}		
	},{});
  }
  
  
  //停用全景項目
  DisableAllView(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'lock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  //啟用全景項目
  EnableAllView(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'unlock',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
  //刪除全景 項目
 delAllView(logintoken,uidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'delete',
			formData : {
				uidList : uidList
			}			
		}		
	},{});
  }
  
  
  videoReload(logintoken,d){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableVideo',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'reload',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				tableSettingUidList : d.tableSettingUidList
			}			
		}		
	},{});  	  
  }
  
  
  videoAllViewReload(logintoken,casinoLocationUidList){
	return this.$http.post(this.url,{
		ns : ns,
		work : 'videoPanoramaSettings',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command:'reload',
			formData : {
				casinoLocationUidList : casinoLocationUidList
			}			
		}		
	},{});  	  
  }
  
    
  //取得賭桌延遲
  getTableVideoDelay(logintoken,casinoLocationUid){
	console.log("casinoLocationUid : ",casinoLocationUid)	;
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableVideoDelay',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				casinoLocationUid : casinoLocationUid				
			}
			
		}		
	},{});
  }
  
  //更新賭桌延遲
  updateTableVideoDelay(logintoken,d){
	
	
	return this.$http.post(this.url,{
		ns : ns,
		work : 'tableVideoDelay',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'modify',
			formData : {
				casinoLocationUid : d.casinoLocationUid,
				tableVideoDelay : d.tableVideoDelay
			}
			
		}		
	},{});
  }
  
  
  
}

export default new VideoManager()