import baseService from './base.js';


let ns = 'gameManagement';


class GameReportManager extends baseService{
  
   constructor() {
        super();
    }
  
  //取得遊戲紀錄 : ByDateTime
  gameResultByDateTime(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'gameResultByDateTime',
				pagination : {
					pageSize : d.pageSize,
					pageIndex : d.pageIndex					
				},
				conditions : {
					filters : {
						themes : d.themes,
						gameSets : d.gameSets,
						tableSpecies : d.tableSpecies						
					},
					where : {
						roundTime : d.roundTime			
					}
				}
				
			}			
		}		
	},{});
  }
  
    //取得遊戲紀錄 : ByRound
  gameResultByRound(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'report',
			formData : {
				name : 'gameResultByRound',
				pagination : d.pagination,
				conditions : {
					filters : {
						themes : d.themes,
						frontCode : d.d_frontCode,
						shoeNo : d.shoeNo,
						roundNo : d.roundNo						
					},
					where : {
						roundTime : {
							start : d.start,
							end : d.end
						}						
					}
				}
				
			}			
		}		
	},{});
  }
  
  //取得遊戲紀錄
  gameResult(logintoken,d,item) {
	
      var re;
	  switch(item)
	  {
		  case ('gameResultByDateTime'):
			re = this.gameResultByDateTime(logintoken,d);
		  break;
		  
		  case ('gameResultByRound'):
			re = this.gameResultByRound(logintoken,d);
		  
		  break;
		  
	  }
	 
	  
	  return re;
 }
 
  //取得遊戲詳細內容 : detail
  gameResultDetail(logintoken,d){
	var url =  this.url;
	
	return this.$http.post(url,{
		ns : ns,
		work : 'gameResult',
		version : '0.0.1',
		loginToken:logintoken,
		data : {
			command : 'detail',
			formData : {
				uid : d.uid				
			}			
		}		
	},{});
  }
   
}

export default new GameReportManager()