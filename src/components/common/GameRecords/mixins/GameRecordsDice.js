export const computed = {
  computed: {
    originGameResult() {
      if (this.current.originGameResult.length > 0) {
        return this.getGameResultDices(this.current.originGameResult)
      } else {
        return []
      }
    },
    modifiedGameResult() {
      if (this.current.modifiedGameResult.length > 0) {
        return this.getGameResultDices(this.current.modifiedGameResult)
      } else {
        return []
      }
    }
  }
}

export const methods = {
  methods: {
    getGameResultDices(source) {
      const result = source.map(x => {
        return { class: `dice-face--${x}`, value: x }
      })
      return result
    },
    setSelected(code) {
      this.selected = code
    },
    openEditModal(identity, index) {
      const originDice = this.modifiedGameResult[index]

      this.selected = originDice.value
      this.defaultSelected = originDice.value

      this.selectedIdentify = identity

      this.selectedIndex = index

      this.isOpenEditModal = true
    },
    closeEditModal() {
      if (this.selected !== this.defaultSelected) {
        this.$set(this.current.modifiedGameResult, this.selectedIndex, this.selected)
      }

      this.resetSelected()
      this.isOpenEditModal = false
    },
    resetSelected() {
      this.selectedIdentify = ""
      this.selectedIndex = ""
      this.selected = ""
      this.defaultSelected = ""
    }
  }
}
