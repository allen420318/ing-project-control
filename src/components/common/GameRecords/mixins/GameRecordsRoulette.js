import Roulette from "@/libs/roulette.js"

export const computed = {
  computed: {
    originGameResult() {
      if (this.current.originGameResult.length > 0) {
        return this.getGameResultRoulette(this.current.originGameResult)
      } else {
        return []
      }
    },
    modifiedGameResult() {
      if (this.current.modifiedGameResult.length > 0) {
        return this.getGameResultRoulette(this.current.modifiedGameResult)
      } else {
        return []
      }
    }
  }
}

export const methods = {
  methods: {
    getGameResultRoulette(source) {
      const value = source[0]
      const color = Roulette.getSectionColor(value) || "green"
      return [{ color, value }]
    },
    setSelected(code) {
      this.selected = code
    },
    openEditModal() {
      const origin = this.modifiedGameResult[0]

      this.selected = origin.value
      this.defaultSelected = origin.value

      this.isOpenEditModal = true
    },
    closeEditModal() {
      if (this.selected !== this.defaultSelected) {
        this.$set(this.current.modifiedGameResult, 0, this.selected)
      }

      this.resetSelected()
      this.isOpenEditModal = false
    },
    resetSelected() {
      this.selected = ""
      this.defaultSelected = ""
    }
  }
}
