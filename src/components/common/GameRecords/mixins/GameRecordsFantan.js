export const computed = {
  computed: {
    originGameResult() {
      if (this.current.originGameResult.length > 0) {
        return this.getGameResultFantan(this.current.originGameResult)
      } else {
        return []
      }
    },
    modifiedGameResult() {
      if (this.current.modifiedGameResult.length > 0) {
        return this.getGameResultFantan(this.current.modifiedGameResult)
      } else {
        return []
      }
    }
  }
}

export const methods = {
  methods: {
    getGameResultFantan(source) {
      return source
    },
    setSelected(code) {
      this.selected = code
    },
    openEditModal() {
      const originValue = this.modifiedGameResult[0]

      this.selected = originValue
      this.defaultSelected = originValue

      this.isOpenEditModal = true
    },
    closeEditModal() {
      if (this.selected !== this.defaultSelected) {
        this.$set(this.current.modifiedGameResult, 0, this.selected)
      }

      this.resetSelected()
      this.isOpenEditModal = false
    },
    resetSelected() {
      this.selected = ""
      this.defaultSelected = ""
    }
  }
}
