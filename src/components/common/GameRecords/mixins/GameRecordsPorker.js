import { mappingCardItems } from "@/libs/poker.js"

export const computed = {
  computed: {
    cardTitleLabels() {
      if (this.isGameTypeBaccarat === true) {
        return { player: ["閒家1", "閒家2", "閒家3"], banker: ["莊家1", "莊家2", "莊家3"] }
      } else if (this.isGameTypeDragonTiger === true) {
        return { player: ["虎"], banker: ["龍"] }
      }
    },
    originGameResult() {
      if (this.current.originGameResult.length > 0) {
        if (this.isGameTypeBaccarat === true) {
          return this.getGameResultCards(this.current.originGameResult)
        } else if (this.isGameTypeDragonTiger === true) {
          return this.getDragonTigerGameResultCards(this.current.originGameResult)
        }
      } else {
        return []
      }
    },
    modifiedGameResult() {
      if (this.current.modifiedGameResult.length > 0) {
        if (this.isGameTypeBaccarat === true) {
          return this.getGameResultCards(this.current.modifiedGameResult)
        } else if (this.isGameTypeDragonTiger === true) {
          return this.getDragonTigerGameResultCards(this.current.modifiedGameResult)
        }
      } else {
        return []
      }
    },
    isGameTypeDragonTiger() {
      return this.current.gameTypeCode === "DRAGONTIGER"
    },
    isGameTypeBaccarat() {
      return this.current.gameTypeCode === "BACCARAT"
    },
    isEnableBlankCard() {
      return this.current.gameTypeCode === "BACCARAT"
    }
  }
}

export const methods = {
  methods: {
    getGameResultCards(source) {
      const convertCards = source.map(mappingCardItems)
      const playerResult = convertCards.slice(0, 3)
      const bankerResult = convertCards.slice(3, 6)

      return { player: playerResult, banker: bankerResult }
    },
    getDragonTigerGameResultCards(source) {
      const convertCards = source.map(mappingCardItems)
      const playerResult = convertCards.slice(1, 2)
      const bankerResult = convertCards.slice(0, 1)

      return { player: playerResult, banker: bankerResult }
    },
    setCardCode(code) {
      this.selectedCardCode = code
    },
    openPorkerCardModal(identity, index) {
      const originCardCode = this.modifiedGameResult[identity][index].originData
      this.selectedCardCode = originCardCode
      this.defaultSelectedCardCode = originCardCode
      this.selectedIdentify = identity

      if (this.isGameTypeBaccarat === true) {
        if (identity === "banker") {
          this.selectedIndex = index + 3
        } else {
          this.selectedIndex = index
        }
      } else {
        if (identity === "banker") {
          this.selectedIndex = index
        } else {
          this.selectedIndex = index + 1
        }
      }

      this.isOpenPorkerCardModal = true
    },
    closePorkerCardModal() {
      if (this.selectedCardCode !== this.defaultSelectedCardCode) {
        this.$set(this.current.modifiedGameResult, this.selectedIndex, this.selectedCardCode)
      }

      this.resetSelectedCard()
      this.isOpenPorkerCardModal = false
    },
    resetSelectedCard() {
      this.selectedIdentify = ""
      this.selectedIndex = ""
      this.selectedCardCode = ""
      this.defaultSelectedCardCode = ""
    }
  }
}
