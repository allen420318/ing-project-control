import { getTopAgentAccountPermissionList } from "@/api/Agent.js"

export const methods = {
  methods: {
    async fetchTopAgentAssociateAccount() {
      try {
        this.isLoading = true

        const payload = {
          topAgentAccount: this.associateAccountInput
        }

        const isAssignSelf = this.associateAccountInput === this.current.topAgentAccount
        if (isAssignSelf === true) return

        const res = await getTopAgentAccountPermissionList(this.loginToken, payload)

        if (res.exitCode === true) {
          if (Array.isArray(res.data) === true) return

          const isDuplicate = this.associateAccountList.filter(x => x.uid === res.data.uid).length > 0
          if (isDuplicate === true) return

          // Sort by account A-Z
          const list = this.associateAccountList
          list.push(res.data)
          this.associateAccountList = list.sort((a, b) => a.account.localeCompare(b.account))
          
          this.associateAccountInput = ""
        } else {
          throw new Error(this.parseErrorMessage(res))
        }
      } catch (err) {
        this.showErrorMessage(err)
      } finally {
        this.isLoading = false
      }
    },
    removeAssociateAccountList(accountUid) {
      const result = this.associateAccountList.filter(x => x.uid !== accountUid)
      this.associateAccountList = result
    }
  }
}

export const watch = {
  watch: {
    associateAccountList(value) {
      if (value.length > 0) {
        const result = value.map(x => x.uid)
        this.current.associateAccounts = result
      } else {
        this.current.associateAccounts = []
      }
    }
  }
}
