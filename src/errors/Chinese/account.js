const errorMessages = new Map([
  //Account 帳號維護相關
  ["AgentGroupIsDisabled", "此帳號已停用"],
  ["AgentKeyIsDisabled", "代理金鑰已被停用"],
  ["AgentCodeHasBeenUsed", "代理識別碼不能使用"],
  ["UserIsNotExist", "使用者不存在"],
  ["MailboxHasBeenUsed", "信箱已被使用"],
  ["UserAccountHasBeenUsed", "帳號已被使用"],
  ["DoNotDisabledSelf", "無法停用自己"],
  ["DoNotDeleteSelf", "無法刪除自己"],
  ["CurrencyIsExist", "貨幣已存在"],
  ["CurrencyUseTaiwan", "貨幣使用台幣"],
  ["UserIsDisabled", "已為停用狀態"],
  ["UserIsLock", "已為上鎖狀態"],
  ["UserIsEnabled", "已為啟用狀態"],
  ["UserIsNotTopAgent", "使用者不是最高代理"],
  ["NotUsedTaiwanCountryCode", "國碼不能使用886"],
  ["CurrencyUnableDelete", "已套用代理，無法刪除"],
  ["CurrencyLastOne", "貨幣剩餘一個，無法刪除"],
  ["LanguageSetCodeNotSet", "未設定進入遊戲預設語系"],
  ["invalidLanguageSetCode", "非法的遊戲預設語系"],
  ["AgentCodeIsRepeat", "代理識別代碼重複"]
])

export default errorMessages
