const errorMessages = new Map([
  //Table 資料表相關
  ["RecordDefaultOption", "資料已是預設選項"],
  ["RecordDeleted", "資料已刪除"],
  ["RecordLocked", "資料已上鎖"],
  ["RecordNotChanged", "資料沒有異動"],
  ["RecordNotExists", "資料不存在"],
  ["RecordNotModify", "資料不可編輯"],
  ["RecordNotDelete", "資料不可刪除"],
  ["RecordRefuse", "資料不可操作"],
  ["RequiredRecordNotExists", "必要欄位不存在"],
  ["RecordUnLocked", "資料未遭上鎖"],
  ["DefaultOptionNotAllowedDelete", "預設資料不允許刪除"],
  ["DefaultOptionNotAllowedLock", "預設資料不允許鎖定"],
  ["IntegrityConstraintViolation", "資料完整性錯誤"],
  ["DataEmpty", "資料為空值"],
  ["ReportDoesNotExist", "報表不存在"],
  ["IndexCrossPage", "指定的索引超過最大頁數"],
  ["RecordCanNotRead", "資料不允許瀏覽"],
  ["RecordIsRepeat", "資料已重複"],
  ["RecordExist", "資料已存在"],
  ["RecordIsEqual", "輸入資料與原資料相同"],
  ["FormatErrorByDate", "開始時間不可在當前時間之前"],
  ["FormatError", "格式錯誤"],
  ["RecordIsRealationOther", "資料有關聯到其他地方，不允許做其他操作"],
  ["RecordBatchErrorByDelete", "資料批次刪除錯誤"],
  ["RecordBatchErrorByLock", "資料批次鎖定錯誤"],
  ["RecordBatchErrorByUnLock", "資料批次解鎖錯誤"],
  ["RecordSaveFail", "存取失敗"],
  ["TableIsOpen", "桌種已開放"],
  ["SiteCodeIsRepeat", "現場桌號已重複"],
  ["FrontCodeIsRepeat", "前台桌號已重複"],
  ["PanomaraAlreadyBindRobotArmTheme", "請先取消關聯手臂間"]
])

export default errorMessages
