import AccountErrorMessages from "./account.js"
import GameErrorMessages from "./game.js"
import LoginErrorMessages from "./login.js"
import RecordTableErrorMessages from "./recordTable.js"
import SystemErrorMessages from "./system.js"
import PermissionErrorMessages from "./permission.js"

const main = new Map([
  ...AccountErrorMessages,
  ...GameErrorMessages,
  ...LoginErrorMessages,
  ...RecordTableErrorMessages,
  ...SystemErrorMessages,
  ...PermissionErrorMessages
])

export default main
