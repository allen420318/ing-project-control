const errorMessages = new Map([
  // Game 遊戲相關
  ["NotAllowedRefunded", "不允許退押注"],
  ["GameHasBeenUsed", "遊戲已被使用, 不允許此操作"],
  ["NoModificationAllowed", "不允許修改賽果"],
  ["ChannelIdIsRepeat", "CDN頻道重複使用"]
])

export default errorMessages
