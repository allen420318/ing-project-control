const errorMessages = new Map([
  //Permission 權限
  ["RoleNameIsBeenUsed", "角色名稱已被使用"],
  ["RoleIsBindSubAgent", "已套用使用者，無法刪除"]
])

export default errorMessages
