import store from "../store/index.js"
import { checkLoginTokenStatus } from "@/api/User.js"

export const checkRoutePermission = targetName => {
  const menu = store.state.menu.menu
  const subMenuList = [].concat(...menu.map(x => x.items))
  const [permissionCheckTarget] = subMenuList.filter(x => x.routeName === targetName)
  let result = true

  if (permissionCheckTarget) {
    permissionCheckTarget.isActive !== true ? (result = false) : null
  }

  // Prevent url redirect loop
  if (targetName === "index") result = undefined

  return result
}

export const checkLoginToken = async loginToken => {
  try {
    const { exitCode } = await checkLoginTokenStatus(loginToken)
    return exitCode === true
  } catch (err) {
    return false
  }
}
