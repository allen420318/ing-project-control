export const getVersionFromUrl = async url => {
  const response = await fetch(url, { headers: { Accept: "text/plain", cache: "no-cache" } })
  return response.text()
}

export const checkVersion = (versionNumber, key = "_ver") => {
  const legacyVersionNumber = localStorage.getItem(key)
  if (legacyVersionNumber !== versionNumber) {
    localStorage.setItem(key, versionNumber)
    return false
  } else {
    return true
  }
}
