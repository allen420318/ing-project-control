import store from "../store/index.js"

export const applyMenuPermission = async () => {
  try {
    // Get Latest menu permissions
    const loginToken = store.state.member.loginToken
    await store.dispatch("menu/setMenuPermissionList", loginToken)

    const menuPermissionList = store.state.menu.menuPermissionList
    const menu = store.state.menu.menu

    if (!Array.isArray(menuPermissionList) || !menuPermissionList.length > 0)
      throw new Error("Not GET Permission Menu from server.")

    // Process with sub menu
    const processedSubMenu = validSubMenu(menu, validSubMenuAPIPermissionHandler)

    // Process with root menu
    const finalMenu = validMenu(processedSubMenu)

    store.dispatch("menu/setMenuList", finalMenu)
  } catch (err) {
    console.log(err)
    throw new Error("Menu permission apply failed")
  }
}

const apiNameExceptionHandling = apiName => {
  const menuPermissionList = store.state.menu.menuPermissionList

  const exceptionList = ["gameVendor", "casinoLocations", "serverSettings"]
  if (apiName === "basic") {
    return menuPermissionList.some(x => exceptionList.includes(x))
  } else {
    return false
  }
}

const validSubMenuAPIPermissionHandler = subMenu => {
  const menuPermissionList = store.state.menu.menuPermissionList

  const result = menuPermissionList.includes(subMenu.apiName) === true || apiNameExceptionHandling(subMenu.apiName)

  const isActive = result === true

  return Object.assign({}, subMenu, { isActive })
}

const validSubMenu = (menu, handler) => {
  const result = menu.map(m => {
    const validSubMenu = m.items.map(handler)

    return Object.assign({}, m, { items: validSubMenu })
  })

  return result
}

const validMenu = menu => {
  const result = menu.map(m => {
    const displayMenuCount = m.items.filter(x => x.isActive === true).length

    const isActive = displayMenuCount > 0

    return Object.assign({}, m, { isActive })
  })

  return result
}
